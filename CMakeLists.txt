cmake_minimum_required(VERSION 3.1.0 FATAL_ERROR)
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)
set(CMAKE_VERBOSE_MAKEFILE OFF)
# Set this on if the machine is Beaglebone black with robotics cape or Beaglebone blue 
set(BEAGLEBONE OFF)

if(UNIX)
  set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pedantic")
endif()
if(WIN32)
  set(CMAKE_CXX_FLAGS "-D_hypot=hypot")
endif()
# Speeding up recompilation
find_program(CCACHE_PROGRAM ccache)
if(CCACHE_PROGRAM)
    set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE "${CCACHE_PROGRAM}")
endif()
project(Devastator VERSION 1.0.0 LANGUAGES CXX)

if(UNIX)
  find_package(Curses)
endif()

if(WIN32)
  set(EIGEN_INCLUDE_DIR "C:\\Program Files\\Eigen3")
  set(COMPILE_DEFINITIONS  _WIN32_WINNT=0x0A00)
endif(WIN32)
find_package(Eigen3 3.3 REQUIRED NO_MODULE)

#set(Boost_DEBUG ON)
set(Boost_USE_STATIC_LIBS ON)
find_package(Boost 1.40 REQUIRED COMPONENTS program_options system serialization)
set(MY_PYTHON_VERSION "3.6")
find_package(Python3 ${MY_PYTHON_VERSION} EXACT REQUIRED COMPONENTS Interpreter Development)
if(Python3_FOUND)
  message(STATUS "Python3 executable found in ${Python3_EXECUTABLE}")
else()
  message(FATAL_ERROR "Python3 not found! Python bindings will not be possible to generate")
endif()
set(PYTHON_EXECUTABLE ${Python3_EXECUTABLE})
set(PYTHON_LIBRARIES ${Python3_LIBRARIES})
set(PYTHON_LIBRARY_DIRS ${Python3_LIBRARY_DIRRS})

################### GOOGLE TEST ###################

# Download and unpack googletest at configure time
configure_file(CMakeLists.txt.in googletest-download/CMakeLists.txt)
execute_process(COMMAND ${CMAKE_COMMAND} -G "${CMAKE_GENERATOR}" .
  RESULT_VARIABLE result
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/googletest-download )
if(result)
  message(FATAL_ERROR "CMake step for googletest failed: ${result}")
endif()
execute_process(COMMAND ${CMAKE_COMMAND} --build .
  RESULT_VARIABLE result
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/googletest-download )
if(result)
  message(FATAL_ERROR "Build step for googletest failed: ${result}")
endif()

# Prevent overriding the parent project's compiler/linker
# settings on Windows
set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)

# Add googletest directly to our build. This defines
# the gtest and gtest_main targets.
add_subdirectory(${CMAKE_CURRENT_BINARY_DIR}/googletest-src
                 ${CMAKE_CURRENT_BINARY_DIR}/googletest-build
                 EXCLUDE_FROM_ALL)

# The gtest/gtest_main targets carry header search path
# dependencies automatically when using CMake 2.8.11 or
# later. Otherwise we have to add them here ourselves.
if (CMAKE_VERSION VERSION_LESS 2.8.11)
  include_directories("${gtest_SOURCE_DIR}/include")
endif()

################### INCLUDE DIRECTORIES ###################

set(DEVASTATOR_THIRD_PARTY_INCLUDE_DIRS ${PROJECT_SOURCE_DIR}/third-party/include ${Boost_INCLUDE_DIR})

if(UNIX)
  set(DEVASTATOR_THIRD_PARTY_INCLUDE_DIRS ${DEVASTATOR_THIRD_PARTY_INCLUDE_DIRS} ${CURSES_INCLUDE_DIR})
endif()

################### SOURCES ###################

set(
    BASE_DEVASTATOR_SOURCES
    ${PROJECT_SOURCE_DIR}/src/ControllerBase.cpp 
    ${PROJECT_SOURCE_DIR}/src/Logger.cpp
    ${PROJECT_SOURCE_DIR}/src/Datatypes.cpp
    ${PROJECT_SOURCE_DIR}/src/Utils.cpp
    ${PROJECT_SOURCE_DIR}/src/NetUtils.cpp
    ${PROJECT_SOURCE_DIR}/src/EKF.cpp
    ${PROJECT_SOURCE_DIR}/src/mock/DCMotor.cpp
    ${PROJECT_SOURCE_DIR}/src/mock/IMU.cpp
    ${PROJECT_SOURCE_DIR}/src/mock/World.cpp
    ${PROJECT_SOURCE_DIR}/src/mock/Simulation.cpp
)

set(
    RC_DEVASTATOR_SOURCES
    ${PROJECT_SOURCE_DIR}/src/rc/DCMotor.cpp
    ${PROJECT_SOURCE_DIR}/src/rc/IMU.cpp
)

################### LIBRARIES ###################

message("Boost_LIBRARIES: ${Boost_LIBRARIES}")
set(THIRD_PARTY_LIBS Eigen3::Eigen ${PYTHON_LIBRARIES} ${Boost_LIBRARIES})

add_library (rc_lib SHARED IMPORTED)
set_target_properties(rc_lib PROPERTIES
  IMPORTED_LOCATION "/usr/lib/librobotcontrol.so.1")

set(DEVASTATOR_LIBS ${THIRD_PARTY_LIBS})

if(WIN32)
  set(DEVASTATOR_SOURCES ${BASE_DEVASTATOR_SOURCES})
  set(DEVASTATOR_LIBS ${DEVASTATOR_LIBS} wsock32 ws2_32)
elseif(NOT BEAGLEBONE)
  set(DEVASTATOR_SOURCES ${BASE_DEVASTATOR_SOURCES})
  set(DEVASTATOR_LIBS ${DEVASTATOR_LIBS} ${CURSES_LIBRARIES})
else()
  set(DEVASTATOR_SOURCES ${BASE_DEVASTATOR_SOURCES} ${RC_DEVASTATOR_SOURCES})
  set(DEVASTATOR_LIBS ${DEVASTATOR_LIBS} ${CURSES_LIBRARIES} rc_lib)
endif()

message("Creating devastator_lib with sources ${DEVASTATOR_SOURCES}")

add_library(devastator_lib ${DEVASTATOR_SOURCES})
target_include_directories(devastator_lib PUBLIC ${PROJECT_SOURCE_DIR}/include SYSTEM PUBLIC ${PYTHON_INCLUDE_DIRS} ${DEVASTATOR_THIRD_PARTY_INCLUDE_DIRS})
target_link_libraries(devastator_lib PUBLIC ${DEVASTATOR_LIBS})
target_compile_definitions(devastator_lib PUBLIC PROJECT_DIR="${PROJECT_SOURCE_DIR}")

# devastatorpy with pybind11
add_subdirectory(third-party/pybind11)
pybind11_add_module(_devpy ${PROJECT_SOURCE_DIR}/src/PythonBindings.cpp)
target_link_libraries(_devpy PRIVATE devastator_lib)
# Copy resulting .pyd or .so file to python directory¨
file(GLOB DEVPY_LIB_FILES
  "${PROJECT_SOURCE_DIR}/build/_devpy*"
)

#file(COPY ${DEVPY_LIB_FILES} DESTINATION ${PROJECT_SOURCE_DIR}/py/devpy)

################### SUB DIRECTORIES ###################

add_subdirectory(src)
add_subdirectory(scripts)
add_subdirectory(tests)