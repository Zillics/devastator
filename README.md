TODO: Clean up and tidy

# Devastator

## Hardware and firmware

### Brain

This assumes that the brain computer is a Beaglebone AI with the am57xx-eMMC-flasher-debian-9.12-iot-armhf-2020-03-01-4gb.img.xz (https://elinux.org/Beagleboard:BeagleBoneBlack_Debian)

`uname -a`

Linux beaglebone 4.14.108-ti-r131 #1stretch SMP PREEMPT Tue Mar 24 19:18:37 UTC 2020 armv7l GNU/Linux 

### Controller

TODO

## Video

### Quick tests
- Stream video to localhost:8090
mjpg_streamer -i "input_uvc.so -d /dev/video0" -o "output_http.so -p 8090 -w /usr/share/mjpg-streamer/www"
- Run classification on video stream and stream to localhost:8090
sudo mjpg_streamer -i "/usr/lib/mjpg-streamer/input_opencv.so -r 640x480 --filter /var/lib/cloud9/BeagleBone/AI/tidl/classification.tidl.so" -o "output_http.so -p 8090 -w /usr/share/mjpg-streamer/www 2> /dev/null"
If it complains after previous successful runs, this might help: ti-mct-heap-check -c

## Setup

### Increase swap

Ensure as large swap is used as possible to prevent compiling getting out of memory.

1. Check how much swap is currently used
`top`
2. Increase swap by N MB and enable it
`sudo mkdir /var/cache/swap`
`sudo dd if=/dev/zero of=/var/cache/swap/swapfile bs=1024k count=N`
`sudo chmod 0600 /var/cache/swap/swapfile`
`sudo mkswap /var/cache/swap/swapfile`
`sudo swapon /var/cache/swap/swapfile`
3. Ensure swap on during boot up
`sudo nano /etc/fstab`
Add line:
`/var/cache/swap/swapfile        none    swap    sw      0       0`

### For fresh BeagleBone Black/AI
- Update kernel (https://beagleboard.org/upgrade)
cd /opt/scripts
git pull
sudo tools/update_kernel.sh
sudo shutdown -r now
- Update distribution components
sudo apt update
sudo apt upgrade

### Dependencies

#### Debian Packages
*With apt*
* sudo apt install cmake nlohmann-json-dev libeigen3-dev libboost-all-dev libssl-dev
#### CMake version 3.17
- Does not exist as precompiled binary as of now for Debian 9, so install from source:
* sudo apt purge cmake
* wget https://github.com/Kitware/CMake/releases/download/v3.17.0/cmake-3.17.0.tar.gz
* tar -xf cmake-3.17.0.tar.gz
* ./bootstrap
* make
* sudo make install
* echo "PATH=\$PATH:/usr/local/bin" >> ~/.bashrc
#### Third party libraries
*pybind11*
* cd devastator
* git clone https://github.com/pybind/pybind11.git

### Network settings
What I learned from messing around with network configuration and dns nameservers:
- Don't mess around with resolv.conf manually
- No need to even touch /etc/network/interfaces if using connmanctl
- Read the manual and spend a little time on understanding. Don't just copy/paste from random posts on stackoverflow

1. Setting static ip addresses for wifi and ethernet
After some headache, connmanctl turned out to work the best. Should have used that in the first place.
Based on the manual: https://manpages.debian.org/testing/connman/connmanctl.1.en.html

- Start connman
`sudo connmanctl`
- List services to find out IDs for your ethernet and wifi
connmanctl> `services`
- Configure static ip addresses and nameservers
connmanctl> `agent on`
connmanctl> `scan wifi`
connmanctl> `connect wifi_<ID>_managed_psk`
connmanctl> `config wifi_<ID>_managed_psk ipv4 manual <CHOOSE_ETHERNET_IP_ADDRESS> 255.255.255.0 192.168.1.1`
connmanctl> `config wifi_<ID>_managed_psk nameservers <PREFERRED_DNS_NAMESERVER1> <PREFERRED_DNS_NAMESERVER2>`
connmanctl> `config wifi_<ID>_managed_psk autoconnect on`
connmanctl> `config ethernet_<ID>_managed_psk ipv4 manual <CHOOSE_WIFI_IP_ADDRESS> 255.255.255.0 192.168.1.1`
connmanctl> `config ethernet_<ID>_managed_psk nameservers 208.67.222.222 208.67.220.220`
connmanctl> `config ethernet_<ID>_managed_psk autoconnect on`
connmanctl> `exit`
- Add your wifi passphrase to connman settings for your recently created wifi service
`sudo nano /var/lib/connman/wifi_107b44e951ce_5a7958454c204e42472d3431384e207632_managed_psk/settings`
TODO: How to encrypt the password instead of hard copy/pasting it to the settings file

2. Update hostname files with aliases for ip addresses
- Add IP addresses with corresponding name to /etc/hosts
- Update desired device name to /etc/hosts and /etc/hostname

## Screen configuration
copy this to ~/.screenrc

```
# Turn off the welcome message
startup_message off

# Disable visual bell
vbell off

# Set scrollback buffer to 10000
defscrollback 10000

# Color
term screen-256color

# Customize the status line
hardstatus alwayslastline
hardstatus string '%{= kG}[ %{G}%H %{g}][%= %{= kw}%?%-Lw%?%{r}(%{W}%n*%f%t%?(%u)%?%{r})%{w}%?%+Lw%?%?%= %{g}][%{B} %m-%d %{W}%c %{g}]'
```

## Testing stuff

### Network communication

*Send message over udp to some port*
echo "message" | nc -w1 -u ip_address port
echo "foo" | nc -w1 -u localhost 2399

*Listen to UDP messages on some port*
netcat -ulp port
netcat -ulp 2399

## Code

## Compilation
### Rules for speedup
- <iostream> 
Use <iosfwd> in header and include <iostream> in cpp files

### Eigen
- Because custom functions for serialization of Eigen matrices have been implemented, all Eigen includes should be done through #include "EigenConfig.hpp". If not done this way, unexpected behavior and cryptic error messages might result. 

# IMU Calibration
Offsets X:  -0.005 Y:   0.020 Z:  -0.004
Scales  X:   1.002 Y:   1.003 Z:   1.010


