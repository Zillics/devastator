set -e # Exit if any command fails
# Build and copy executables to bin/ dir
mkdir -p build
cd build
cmake ..
make
cd ../
python3 setup.py install
