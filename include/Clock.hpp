#pragma once

#include <chrono>
#include <ctime>
#include <iostream>
#include <array>

#define N_TIMERS 5

using namespace std::chrono;

struct Clock {
    Clock() : t0(high_resolution_clock::now()) {}
    uint64_t t() {
	    auto t = high_resolution_clock::now();
        uint64_t micros = duration_cast<microseconds>(t - t0).count();
	    return micros;
    }
    uint64_t t_millis() {
        auto t = high_resolution_clock::now();
        uint64_t millis = duration_cast<milliseconds>(t - t0).count();
        return millis;
    }
    void tic(int i) {
        m_t[i] = high_resolution_clock::now();
    }
    uint64_t toc(int i) {
        auto t2 = high_resolution_clock::now();
        uint64_t T = duration_cast<microseconds>(t2 - m_t[i]).count();
        return T;
    }
    uint64_t toc(int i,const std::string& measurand) {
        auto T = toc(i);
        std::cout << "[Clock] " << measurand << " took " << T << " microseconds\n";
        return T;
    }
    std::chrono::high_resolution_clock::time_point t0;
private:
    std::array<std::chrono::high_resolution_clock::time_point,N_TIMERS> m_t;
};
