#pragma once

#include <iostream>
#include <string>
#include <algorithm>
#include <chrono>
#include <ctime>
#include <math.h>
#include <memory>
#include <exception>
#include <limits>
#include <json.hpp>
#include "Parameters.hpp"
#include "EigenConfig.hpp"
#include "Datatypes.hpp"
#include "Clock.hpp"
#include "Utils.hpp"
#include "NetUtils.hpp"
#include "EKF.hpp"
#include "RobotComponents.hpp"

using json = nlohmann::json;

class ControllerBase {
public:
    ControllerBase(IMU& imu, DCMotor& leftMotor, DCMotor& rightMotor, const std::string& controlConfigFilename, const std::string& netConfigFilename);
    ControllerBase(IMU& imu, DCMotor& leftMotor, DCMotor& rightMotor, json controlParams, json netParams);
    virtual ~ControllerBase() {}
    /* One control cycle */
    void tick();
    void tick(double interval_s);
    /* Run for duration */
    void run(int freq, int duration_s);
    /* Update all sensor values */
    void updateSensors();
    /* Update state values */
    void updateState(); 
    /* Set the control value that will be sent to controller on next tick */
    void setControl(ControlCmd& cmd);
    void setLeftTrack(double speed);
    void setRightTrack(double speed);
    void setTracks(double lSpeed, double rSpeed);
    void printState() const { m_state.print(); }
    void printSensors() const { m_sensor.print(); }
	SensorState getSensors() const { return m_sensor; }
	RobotState getState() const { return m_state; }
	ControlCmd getControls() const { return m_controls; }
    // Test function. remove
    bool hasReceived();
    /* Return current time in microseconds */
    uint64_t t() { return m_clock.t(); }
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
private:
    void setup();
    IMU& m_imu;
	DCMotor& m_leftMotor;
	DCMotor& m_rightMotor;
    enum TimerType {ekf_timer, tick_timer, debug_timer};
    Clock m_clock;
    ControlCmd m_controls;
    SensorState m_sensor;
    RobotState m_state;
    UdpSubscriber m_controlSub;
    UdpPublisher m_statePub;
    UdpPublisher m_sensorPub;
    double m_imuCalibDuration;
    int m_imuCalibNumSamples;
    EKF m_ekf;
};
