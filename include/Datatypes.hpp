#pragma once
#include <fstream>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/array.hpp>
#include "EigenConfig.hpp"
#include <iostream>

struct DataType {
    virtual void print() const = 0;
    virtual void log(std::ofstream& ofs) const = 0;
    double t;
};

struct ControlCmd : public DataType {
    ControlCmd() : u(Eigen::Vector2d::Zero()) {}
	ControlCmd(double lt, double rt) {
		u << lt, rt;
	}
	void print() const;
	void log(std::ofstream& ofs) const;
	//Eigen::Vector2d& getControls() { return u; }
	Eigen::Vector2d u;
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
private:
	/* (left,right) velocity */
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive& ar, const unsigned int version) {
		ar & u;
	}
};

struct RobotState : public DataType {
	RobotState() :	 s(Eigen::Matrix<double,6,1>::Zero())
					,u(Eigen::Vector2d::Zero())
					,z(Eigen::Vector4d::Zero())
					{}
	Eigen::Vector2d pos() const { return Eigen::Vector2d(s(0),s(1)); }
	Eigen::Vector2d vel() const { return Eigen::Vector2d(s(3),s(4)); }
	double angle() const { return s(2); }
	double angularVel() const { return s(5); }
	void print() const;
    void log(std::ofstream& ofs) const;
	Eigen::Matrix<double, 6, 1> s;
	Eigen::Vector2d u;
	/* x_accel, y_accel, angle (rad), angular velocity (rad/s) */
	Eigen::Vector4d z;
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
private:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive &ar, const unsigned int version) {
		ar & s;
		ar & z;
		ar & u;
	}
};

struct SensorState : public DataType {
	SensorState() : z(Eigen::Vector4d::Zero()) {}
	SensorState(Eigen::Vector4d& init) : z(init) {}
	double angle() const { return z(2); }
	void print() const;
    void log(std::ofstream& ofs) const;
	Eigen::Vector4d z;
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
private:
	friend class boost::serialization::access;
	template<class Archive>
	void serialize(Archive &ar, const unsigned int version) {
		ar & z;
	}
};
