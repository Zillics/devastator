#pragma once
#include "EigenConfig.hpp"
#include <assert.h>
#include <math.h>
#include <vector>
#include "Utils.hpp"
#include <iostream>

using json = nlohmann::json;

/*
Model of differential drive robot equipped with IMU sensor

state:      (x,y,theta,v_x,v_y,omega)
control:    (v_L,v_R)
sensor:     (v_x',v_y')
    where
x,y: robot position (world coordinates)
v_x,v_y: robot speed (world coordinates)
theta: angle
omega: angular velocity
v_L , v_R: left/right wheel speed
v_x', v_y' = robot speed (robot coordinates)
*/
// TODO: Wrapper class for model allowing multiple variations of a model
class Model {
public:
    Model(json params) : Model(params.value("radius",-1.0), params.value("sensor_rotation_deg",0.0)) {}
    Model(double radius, double sensorRotation) : m_r(radius)
                                                , m_sensorRotation(sensorRotation) 
    {
        assert(m_r > 0.0);
        m_xN = 6;
        m_uN = 2;
        m_zN = 4;
    }
    ~Model() {}
    /* x_hat = f(x,u) */
    Eigen::VectorXd prediction(Eigen::VectorXd& prevState, Eigen::VectorXd& control, double dt);
    /* y_hat = z - h(^x) */
    Eigen::VectorXd innovation(Eigen::VectorXd& observation, Eigen::VectorXd& prediction);
    /* F */
    Eigen::MatrixXd stateJacobian(Eigen::VectorXd& x, Eigen::VectorXd& u, double dt);
    /* H */
    Eigen::MatrixXd observationJacobian(Eigen::VectorXd& x, double dt);
    int getStateDim() const { return m_xN; }
    int getControlDim() const { return m_uN; }
    int getObservationDim() const { return m_zN; }
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
private:
    int m_xN;
    int m_uN;
    int m_zN;
    double m_r;
    double m_sensorRotation;
};

/* Extended Kalman filter */
class EKF {
public:
    EKF(json params) : EKF( params["model"],
                            params.value("state_variances",std::vector<double>()),
                            params.value("observation_variances",std::vector<double>()),
                            params.value("print_steps",false),
                            params.value("print_debug",false) ) 
                        {}
    EKF(json modelParams, const std::vector<double>& q, const std::vector<double>& r, bool printSteps, bool printDebug);
    ~EKF() {}
    void initialize(Eigen::VectorXd& x0);
    void step(Eigen::VectorXd u, Eigen::VectorXd z, double dt);
    void getState(Eigen::VectorXd& x) { x = m_x; }
    Eigen::VectorXd getState() const { return m_x; }
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
private:
    void printStep(Eigen::VectorXd& x, Eigen::MatrixXd& P, Eigen::VectorXd& u, Eigen::VectorXd& z, double dt) const;
    Model m_model;
    /* Number of state variables */
    unsigned int m_xN;
    /* Number of control variables */
    unsigned int m_uN;
    /* Number of observation variables */
    unsigned int m_zN;
    /* State */
    Eigen::VectorXd m_x;
    /* Covariance matrix */
    Eigen::MatrixXd m_P;
    /* Process covariance matrix */
    Eigen::MatrixXd m_Q;
    /* Observation covariance matrix */
    Eigen::MatrixXd m_R;
    /* Identity matrix */
    Eigen::MatrixXd m_I;
    /* Indicates whether printing is on/off */
    bool m_printSteps;
    bool m_printDebug;
    int m_iStep;
};
