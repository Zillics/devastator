#pragma once

#include <fstream>
#include <string>
#include "Datatypes.hpp"

struct Log {
    std::string filename;
    std::ofstream filestream;
    Log(std::string& filename);
    Log() : filename(""), filestream() {}
    ~Log();
    void close() { filestream.close(); }
    bool open(std::string& _filename);
};

class CSVLogger {
public:
    CSVLogger(std::string root_dir);
    ~CSVLogger();
    bool setControlLog(std::string& filename);
    bool setSensorLog(std::string& filename);
    bool setStateLog(std::string& filename);
    bool logControls(ControlCmd& cmd);
    bool logSensors(SensorState& sensor);
    bool logState(RobotState& state);
private:
    std::string m_root_dir;
    bool _log(DataType& data, Log& logObj);
    Log m_controlLog;
    Log m_sensorLog;
    Log m_stateLog;
};