#pragma once

#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/bind.hpp>
#include <iostream>
#include <string>
#include <sstream>
#include <thread>
#include <atomic>
#include <chrono>
#include <exception>
#include <stdexcept>
#include "json.hpp"
#include "Utils.hpp"
#include "Datatypes.hpp"
#include "Clock.hpp"

#define RECV_BUFFER_SIZE 1024
#define DEBUG_UDP 0

using boost::asio::ip::udp;
using boost::asio::ip::address;
using json = nlohmann::json;

class UdpPublisher {
public:
    UdpPublisher(const std::string& filename);
    UdpPublisher(json params);
    UdpPublisher(const std::string& address, int port, const std::string& name="default_name");
    ~UdpPublisher();
    void publish(const std::string& data);
    // TODO: Implement only one publish class for DataType
    void publish(ControlCmd& data);
    void publish(RobotState& data);
    void publish(SensorState& data);
private:
    std::string m_name;
    std::string m_address;
    int m_port;
    boost::asio::io_service m_service;
    udp::socket m_socket;
    udp::endpoint m_endpoint;
};

class UdpSubscriber {
public:
    UdpSubscriber(const std::string& filename);
    UdpSubscriber(json params);
    UdpSubscriber(const std::string& address, int port, const std::string& name="default_name");
    ~UdpSubscriber();
    /** Start receiving packets */
    void start();
    /** Stop receiving packets */
    void stop();
    /** Returns true if new packet has been received since last read */
    bool hasReceived() { return m_received; }
    /** Serialize receive buffer into given ControlCmd object. Returns false if no data is available yet */
    bool read(ControlCmd& data);
    /** Serialize receive buffer into given SensorState object. Returns false if no data is available yet */
    bool read(SensorState& data);
    /** Serialize receive buffer into given RobotState object. Returns false if no data is available yet */
    bool read(RobotState& data);
private:
    void receiveHandler(const boost::system::error_code& error, size_t bytes_transferred);
    void runIO();
    std::string m_name;
    std::string m_address;
    int m_port;
    boost::asio::io_service m_service;
    udp::socket m_socket;
    udp::endpoint m_receiverEndpoint;
    udp::endpoint m_senderEndpoint;
    boost::array<char, RECV_BUFFER_SIZE> m_buffer;
    size_t m_nBytes;
    bool m_received;
    enum ioState {ON, OFF};
    std::atomic<ioState> m_ioState;
    std::atomic<bool> m_shutDown;
    std::thread m_ioThread;
    std::exception_ptr m_globalExceptionPtr;
    Clock m_clock;
};