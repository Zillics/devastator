#pragma once

// RC parameters
#define I2C_BUS 2
#define GPIO_INT_PIN_CHIP 3
#define GPIO_INT_PIN_PIN  21

// Other parameters
#define DEFAULT_SAMPLERATE 200 // Admissible values: 200,100,50,40,25,20,10,8,5,4
#define DEFAULT_NSAMPLES 40 // Number of samples used for mean value of sensors 

// H-bridge motors
#define LEFTMOTOR_CH 4
#define RIGHTMOTOR_CH 1
#define FANMOTOR_CH 2
