#pragma once
#include <string>
#include <memory>
#include <json.hpp>
#include "Clock.hpp"
#include "Parameters.hpp"
#include "EigenConfig.hpp"

using json = nlohmann::json;

/* Sensors we are interested in: (acc_x,acc_y,theta_z,omega_z) */
/* theta = angle, omega = angular velocity */
class IMU {
    public:
        IMU(int sampleRate) : m_sampleRate(sampleRate) { assert(m_sampleRate > 0); };
        virtual ~IMU() {}
        virtual bool setup() { return true; };
        virtual void autoCalibrate(double duration_ms, int numSamples){}
        virtual void getReadings(Eigen::Vector4d& readings){}
        virtual void setParams(json params) {}
    protected:
        int m_sampleRate;
};

/* Base classes for all components that Controller is using */
class DCMotor {
public:
    DCMotor() : m_name("Unnamed") {}
    DCMotor(std::string name) : m_name(name) {}
    virtual ~DCMotor() {}
    /** Drives motor at given dutyCycle (-1.0 ... 1.0). Returns false if something failed */
    virtual bool drive(double dutyCycle) const { return true; };
    /** Connects the motor terminal pairs together which makes the motor fight against its own back EMF turning it into a brake resisting rotation. */
    virtual bool brake() const { return true; };
    /** Returns name of motor */
    std::string name() const { return m_name; }
    /** Set maximum absolute value for duty cycle. Duty cycle will be normalized with respect to this value */
    virtual void setLimit(double limit) {};
protected:
    /** Name of motor */
    std::string m_name;
};

