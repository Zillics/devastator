#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include "EigenConfig.hpp"
#include "json.hpp"

#define PI 3.14159265

using json = nlohmann::json;

json openJson(const std::string& filename);
double degToRad(int deg);
Eigen::Matrix3d rot_x(double rad_rot);
Eigen::Matrix3d rot_z(double rad_rot);
Eigen::Matrix3d rot_y(double rad_rot);