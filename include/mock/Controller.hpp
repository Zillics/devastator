#pragma once

#include "mock/DCMotor.hpp"
#include "mock/IMU.hpp"
#include "Utils.hpp"
#include "Parameters.hpp"
#include "RobotComponents.hpp"
#include "ControllerBase.hpp"
#include "mock/World.hpp"

using json = nlohmann::json;

namespace mock {

class Controller : public virtual ::ControllerBase {
public:
    Controller(const std::string& controlConfigFilename, const std::string& netConfigFilename, std::shared_ptr<World> world)
            : Controller(openJson(controlConfigFilename), openJson(netConfigFilename), world)
    {}
    Controller(json controlParams, json netParams, std::shared_ptr<World> world)
            : ControllerBase(*(new IMU(controlParams["IMU"], world->robot())), *(new DCMotor("left",LEFTMOTOR_CH, world->robot()->leftWheel())), *(new DCMotor("right",RIGHTMOTOR_CH, world->robot()->rightWheel())), controlParams, netParams)
            , m_world(world)
            {
                std::cout       << "[Mock::Controller] Controller created with parameters:\n" 
                                << controlParams.dump()
                                << '\n'
                                << netParams.dump()
                                << '\n';
            }
private:
    std::shared_ptr<World> m_world;
};

} // namespace mock