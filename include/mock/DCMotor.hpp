#pragma once

#include <iostream>
#include <string>
#include <cmath>
#include <memory>
#include "mock/World.hpp"
#include "RobotComponents.hpp"

using json = nlohmann::json;

namespace mock {

/* Mock class for DC motor */
class DCMotor : public ::DCMotor {
public:
    DCMotor() : m_channel(-1) {}
    DCMotor(std::string name, int channel, std::shared_ptr<Wheel> wheel);
    ~DCMotor() override;
    /** Drives motor at given dutyCycle (-1.0 ... 1.0). Returns false if something failed */
    bool drive(double dutyCycle) const override;
    /** Connects the motor terminal pairs together which makes the motor fight against its own back EMF turning it into a brake resisting rotation. */
    bool brake() const override;
    /** Set maximum absolute value for duty cycle. Duty cycle will be normalized with respect to this value */
    void setLimit(double limit) override;
private:
    /** Motor channel (0 - 3) */
    int m_channel;
    /** Is motor ready drive? */
    bool m_enabled;
    /** Maximum allowed duty cycle */
    double m_limit;
    std::shared_ptr<Wheel> m_wheel;
};

} // namespace mock
