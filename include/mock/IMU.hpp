#pragma once

#include <deque>
#include <queue>
#include <memory>
#include <math.h>
#include <iostream>
#include <string>
#include <algorithm>
#include <array>
#include <thread>
#include <atomic>
#include <random>
#include <chrono>
#include <exception>
#include "EigenConfig.hpp"
#include "json.hpp"
#include "Clock.hpp"
#include "Datatypes.hpp"
#include "Parameters.hpp"
#include "RobotComponents.hpp"
#include "World.hpp"

using json = nlohmann::json;

namespace mock {

// A mock version of the rc_mpu_data_t struct of Robotics cape library
struct mock_mpu_data_t {
    mock_mpu_data_t()   : accel({0,0,0})
                        , gyro({0,0,0})
                        , compass_heading(0.0)
                        {}
    void print() { 
        for(const auto& a_i : accel) {
            std::cout << a_i << ",";
        }
        for(const auto& g_i : gyro) {
            std::cout << g_i << ",";
        }
        std::cout << compass_heading << '\n';
    }
    std::array<double, 3> accel;
    std::array<double, 3> gyro;
    double compass_heading;
private:
};

/* Mock class for IMU. Mirrors rc::IMU */
/* Sensors we are interested in: (acc_x,acc_y,theta_z,omega_z) */
/* theta = angle, omega = angular velocity */
class IMU : public ::IMU {
    public:
        IMU(json params, std::shared_ptr<TankRobot> robot);
        ~IMU() override {}
        bool setup() override;
        void autoCalibrate(double duration_ms, int numSamples) override;
        void getReadings(Eigen::Vector4d& readings) override;
        void setParams(json params) override;        
    private:
        static double noise(double mean, double std);
	    static std::normal_distribution<double> m_noisePdf;
        static std::default_random_engine m_generator;
        void update();
        mock_mpu_data_t m_data;
        Eigen::Vector4d m_mean;
        Eigen::Vector4d m_bias;
        double m_accelNoiseStd;
        double m_gyroNoiseStd;
        // TODO: More beautiful implementation
        std::queue<double> m_accelXReadings;
        std::queue<double> m_accelYReadings;
        std::queue<double> m_thetaZReadings;
        std::queue<double> m_omegaZReadings;
        std::queue<double> m_timeInstances;
        double runningMean(double oldest, double newest, double prevMean, int N) { return prevMean + (newest-oldest)/N; }
        //static double runningMean(Eigen::VectorXd oldest, Eigen::VectorXd newest, Eigen::VectorXd prevMean, int N) { return prevMean + (newest-oldest)/N; } TODO
        void dmpCallback(void);
        void runDmp();
        enum TimerType {calib_timer, dt_timer};
        Clock m_clock;
        double m_dt;
        std::thread m_dmpThread;
        std::atomic<bool> m_shutDown;
        std::shared_ptr<TankRobot> m_robot;
    };

} // namespace mock