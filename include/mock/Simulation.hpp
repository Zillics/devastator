#pragma once
#include <json.hpp>
#include <thread>
#include <atomic>
#include <assert.h>
#include <memory>
#include "Utils.hpp"
#include "World.hpp"
#include "mock/Controller.hpp"
#include "Clock.hpp"

using json = nlohmann::json;

class Simulation {
public:
    Simulation(const std::string& filename);
    Simulation(json params);
    ~Simulation();
    void run(int duration_s);
private:
    void runWorld(double freq);
    void runController(double freq);
    std::atomic<bool> m_shutDown;
    std::atomic<bool> m_runWorld;
    std::atomic<bool> m_runController;
    double m_worldFreq;
    double m_controllerFreq;
    uint64_t m_interval_micros;
    std::shared_ptr<World> m_world;
    mock::Controller m_controller;
    std::thread m_worldThread;
    std::thread m_controllerThread;
    Clock m_clock;
};