#pragma once
#include "EigenConfig.hpp"
#include <json.hpp>
#include <boost/algorithm/clamp.hpp>
#include <math.h>
#include <thread>
#include "Clock.hpp"
#include <iostream>
using json = nlohmann::json;

/* Simulation of motorized wheel */
class Wheel {
public:
    Wheel(json params);
    ~Wheel() {}
    // TODO: Add a little gaussian noise?
    void drive(double dutyCycle) { m_dutyCycle = dutyCycle; }
    void tick(double dt);
    double vel() const { return m_vel; }
private:
    double m_vel;       // tangential velocity
    double m_acc;       // tangential acceleration
    double m_maxVel;    // maximum tangential velocity    
    double m_maxAcc;    // maximum tangential acceleration
    double m_dutyCycle;    
};

struct State {
    State();
    void print() const;
    Eigen::Vector3d pos;
    Eigen::Vector3d vel;
    Eigen::Vector3d acc;
    Eigen::Vector3d angle;
    Eigen::Vector3d angular_v;
    Eigen::Vector3d angular_a;
};

/* Simulation of differential drive type tank robot */
class TankRobot {
public:
    TankRobot(json params);
    ~TankRobot() {}
    void print() const { m_state.print(); }
    void tick(double dt);
    void updateState(double dt);
    const Eigen::Vector3d& angle() const { return m_state.angle; }
    const Eigen::Vector3d& vel() const { return m_state.vel; }
    const Eigen::Vector3d& acc() const { return m_state.acc; }
    const Eigen::Vector3d& omega() const { return m_state.angular_v; }
    std::shared_ptr<Wheel> leftWheel() { return m_leftWheel; }
    std::shared_ptr<Wheel> rightWheel() { return m_rightWheel; }
private:
    double m_r; // radius = half of distance between wheels
    State m_state;
    std::shared_ptr<Wheel> m_leftWheel;
    std::shared_ptr<Wheel> m_rightWheel;
};

/*
    Simulated world representing ground truth for all mock components
*/
class World {
public:
    World(json params);
    ~World() {}
    double t() { return m_clock.t(); }
    void tick(double dt);
    void run(double freq_hz, double duration_s);
    std::shared_ptr<TankRobot> robot() { return m_robot; }
private:
    Clock m_clock;
    std::shared_ptr<TankRobot> m_robot;
};
