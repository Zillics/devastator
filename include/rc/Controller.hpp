#pragma once

#include "ControllerBase.hpp"
#include "rc/DCMotor.hpp"
#include "rc/IMU.hpp"

using json = nlohmann::json;

namespace rc {

class Controller : public ::ControllerBase {
public:
    Controller(const std::string& controlConfigFilename, const std::string& netConfigFilename)
            : ::Controller(*(new IMU), *(new DCMotor), *(new DCMotor), controlConfigFilename, netConfigFilename) {}
    Controller(json controlParams, json netParams)
            : ::Controller(*(new IMU), *(new DCMotor), *(new DCMotor), controlParams, netParams) {}
};

} // namespace rc