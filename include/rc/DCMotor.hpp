#pragma once

#include <rc/motor.h>
#include <rc/time.h>
#include <iostream>
#include <string>
#include <cmath>
#include "RobotComponents.hpp"

namespace rc {

/* Class for H-bridge motors of Robotics cape */
class DCMotor : public ::DCMotor{
public:
    DCMotor(std::string name, int channel);
    ~DCMotor() override;
    /** Drives motor at given dutyCycle (-1.0 ... 1.0). Returns false if something failed */
    bool drive(double dutyCycle) const override;
    /** Connects the motor terminal pairs together which makes the motor fight against its own back EMF turning it into a brake resisting rotation. */
    bool brake() const override;
    /** Set maximum absolute value for duty cycle. Duty cycle will be normalized with respect to this value */
    void setLimit(double limit) override;
private:
    /** Initializes all 4 motors and leaves them in a free-spin (0 throttle) state. */
    bool initializeMotors() const;
    /** Puts all 4 motors into a free-spin (0 throttle) state, puts the h-bridges into standby mode, and closes all file pointers to GPIO and PWM systems. */    
    bool shutDownMotors() const;
    /** Flag for whether or not rc_motor_init has been executed */
    static bool rc_initialized;
    /** Total number of Motor objects */
    static int nMotors;
    /** Motor channel (0 - 3) */
    int m_channel;
    /** Is motor ready drive? */
    bool m_enabled;
    /** Maximum allowed duty cycle */
    double m_limit;
};

} // namespace rc
