#pragma once

#include <deque>
#include <queue>
#include <memory>
#include <math.h>
#include <iostream>
#include <string>
#include <algorithm>
#include <rc/mpu.h>
#include <exception>
#include "EigenConfig.hpp"
#include "json.hpp"
#include "Clock.hpp"
#include "Datatypes.hpp"
#include "Parameters.hpp"
#include "RobotComponents.hpp"

using json = nlohmann::json;

/* Class for IMU of Robotics Cape */
/* Sensors we are interested in: (acc_x,acc_y,theta_z,omega_z) */
/* theta = angle, omega = angular velocity */
namespace rc {
class IMU : public ::IMU {
    public:
        IMU();
        ~IMU() override {}
        bool setup() override;
        void autoCalibrate(double duration_ms, int numSamples) override;
        void getReadings(Eigen::Vector4d& readings) override;
        void setParams(json params) override;
    private:
        static rc_mpu_data_t m_data;
        static rc_mpu_config_t m_conf;
        // Eigen::Vector4d = Eigen::Matrix<double,1,4>
        static Eigen::Vector4d m_mean;
        static Eigen::Vector4d m_bias;
        // TODO: More beautiful implementation
        static std::queue<double> m_accelXReadings;
        static std::queue<double> m_accelYReadings;
        static std::queue<double> m_thetaZReadings;
        static std::queue<double> m_omegaZReadings;
        static std::queue<double> m_timeInstances;
        static double runningMean(double oldest, double newest, double prevMean, int N) { return prevMean + (newest-oldest)/N; }
        //static double runningMean(Eigen::VectorXd oldest, Eigen::VectorXd newest, Eigen::VectorXd prevMean, int N) { return prevMean + (newest-oldest)/N; } TODO
        static void dmpCallback(void);
        static m_clock;
        static double m_dt;
    };

} // namespace rc