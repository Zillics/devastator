from devpy import UdpPublisher, UdpSubscriber, ControlCmd, SensorState
import json
import time

class Brain:
    def __init__(self, brainConfigFilename, networkConfigFilename):
        print("Brain created")
        with open(networkConfigFilename) as f:
            netParams = json.load(f)
        with open(brainConfigFilename) as f:
            brainParams = json.load(f)

        subParams = netParams['sensor_udp']
        pubParams = netParams['control_udp']
        self.sensorSub = UdpSubscriber(subParams['ip'], subParams['port'], subParams.get('name', 'default_name'))
        self.controlPub = UdpPublisher(pubParams['ip'], pubParams['port'], pubParams.get('name', 'default_name'))
        self.sensorSub.start()
        self.sensorReadings = SensorState()
        self.controls = ControlCmd()
        self.interval_ms = brainParams['interval_ms']

    def tick(self):
        self.controlPub.publish(self.controls)
        #if(self.sensorSub.hasReceived()):
        #    self.sensorSub.readSensor(self.sensorReadings)

    def run(self):
        self.tick()
        #time.sleep(self.interval_ms // 1000)

    """ 
    left,right: float [-1.0,1.0]. Set duty cycle of both left and right motors
    """
    def drive(self,left,right):
        self.controls.leftTrack = left
        self.controls.rightTrack = right
