from devpy import UdpSubscriber, RobotState
import json
import requests
import argparse
from sys import exit
from signal import signal, SIGINT
from flask import jsonify

def handler(signal_received, frame):
    # Handle any cleanup here
    print('SIGINT or CTRL-C detected. Exiting gracefully')
    exit(0)

parser = argparse.ArgumentParser(description='Publish some devastator data over UDP')
parser.add_argument('--ip', dest='ip', action='store', default='127.0.0.1', help='IP address')
parser.add_argument('--port', dest='port', action='store', default=8001, help='Port number')
args = parser.parse_args()

if __name__ == '__main__':

    ip = args.ip
    port = args.port
    endpoint = "http://{}:{}/data/state".format(ip,port)
    print("Endpoint: ", endpoint)
    sub = UdpSubscriber("127.0.0.1", 6030,"dummy_subscriber")
    sub.start()
    state = RobotState()
    signal(SIGINT, handler)
    while(True):
        if(sub.hasReceived()):
            sub.readState(state)
            s = state.s.tolist()
            u = state.u.tolist()
            z = state.z.tolist()
            data = {'s':s,'u':u,'z':z}
            r = requests.post(endpoint, data=json.dumps(data))
