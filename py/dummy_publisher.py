from devpy import UdpPublisher, ControlCmd, SensorState, RobotState
import json
import time
from signal import signal, SIGINT
from sys import exit
import argparse

def handler(signal_received, frame):
    # Handle any cleanup here
    print('SIGINT or CTRL-C detected. Exiting gracefully')
    exit(0)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Publish some devastator data over UDP')
    parser.add_argument('--ip', dest='ip', action='store', default='127.0.0.1', help='IP address')
    parser.add_argument('--port', dest='port', action='store', default=6666, help='Port number')
    parser.add_argument('--type', dest='type', action='store', default='c', help='Type of published data (c=ControlCmd,s=SensorState,r=RobotState)')
    parser.add_argument('--freq', dest='freq', action='store', default=100, help='Publishing frequency in Hz')
    parser.add_argument('--data',action='store', nargs='*', type=float, default=[0,0], help='Data to publish (only for ControlCmd for now)')
    args = parser.parse_args()

    if(args.type=='c'):
        assert(len(args.data) == 2)
        data = ControlCmd(args.data[0],args.data[1])
        print("Publishing ControlCmd...")
        data.print()
    elif(args.type=='s'):
        print("Publishing SensorState...")
        data = SensorState()
        data.print()
    elif(args.type=='r'):
        print("Publishing RobotState...")
        data = RobotState()
        data.print()
    else:
        print("Wrong --type argument. Acceptable values are c or s")
        exit(-1)

    pub = UdpPublisher(args.ip,int(args.port),"dummy_publisher")
    print('Running. Press CTRL-C to exit.')
    count = 0

    signal(SIGINT, handler)
    t = 1/float(args.freq)
    while True:
        pub.publish(data)
        time.sleep(t)
