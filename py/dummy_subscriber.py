from devpy import UdpPublisher, UdpSubscriber, ControlCmd, SensorState, RobotState
import json
import time
from signal import signal, SIGINT
from sys import exit
import argparse

def handler(signal_received, frame):
    # Handle any cleanup here
    print('SIGINT or CTRL-C detected. Exiting gracefully')
    exit(0)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Subscribe to some devastator data over UDP')
    parser.add_argument('--ip', dest='ip', action='store', default='127.0.0.1', help='IP address')
    parser.add_argument('--port', dest='port', action='store', default=6666, help='Port number')
    parser.add_argument('--type', dest='type', action='store', default='c', help='Type of data subscribed to (c=ControlCmd or s=SensorState, r=RobotState)')
    args = parser.parse_args()

    sub = UdpSubscriber(args.ip,int(args.port),"dummy_subscriber")
    sub.start()
    time.sleep(1)
    signal(SIGINT, handler)
    count = 0
    if(args.type=='c'):
        print("Subscribing to ControlCmd....")
        data = ControlCmd()
        print('Running. Press CTRL-C to exit.')
        while True:
            if(sub.hasReceived()):
                success = sub.readCmd(data)
                print("<<<<<<<<<<<<<< ", count)
                count += 1
                data.print()
    elif(args.type=='s'):
        print("Subscribing to SensorState....")
        data = SensorState()
        while True:
            if(sub.hasReceived()):
                success = sub.readSensor(data)
                print("<<<<<<<<<<<<<< ", count)
                count += 1
                data.print()
    elif(args.type=='r'):
        print("Subscribing to RobotState....")
        data = RobotState()
        while True:
            if(sub.hasReceived()):
                success = sub.readState(data)
                print("<<<<<<<<<<<<<< ", count)
                count += 1
                data.print()

    else:
        print("Wrong --type argument. Acceptable values are c or s")
        exit(-1)
