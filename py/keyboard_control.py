from __future__ import print_function
import sys, select, termios, tty, os
import numpy as np
import time
import threading
from Brain import Brain

"""
keyboard_control.py
This is a modified ROS-less version of teleop_twist_keyboard.py 
(https://github.com/ros-teleop/teleop_twist_keyboard/blob/master/teleop_twist_keyboard.py)
TODO: Make non-blocking so that timeouts work
"""

msg = """
Control brain with keyboard
---------------------------
Moving around:
   u    i    o
   j    k    l
   m    ,    .
For Holonomic mode (strafing), hold down the shift key:
---------------------------
   U    I    O
   J    K    L
   M    <    >
t : up (+z)
b : down (-z)
anything else : stop
q/z : increase/decrease max speeds by 10%
w/x : increase/decrease only linear speed by 10%
e/c : increase/decrease only angular speed by 10%
CTRL-C to quit
"""

moveBindings = {
        'y':(1,1),
        'b':(-1,-1),
        'q':(1,0),
        'z':(-1,0),
        'å':(0,1),
        '-':(0,-1),
    }

speedBindings = {
        's':(1.01,1.0),
        'x':(.99,1.0),
        'd':(1.01,1.01),
        'c':(0.99,0.99),
        'f':(1.0,1.01),
        'v':(1.0,0.99),
    }

pwd = os.path.dirname(os.path.realpath(__file__))
cmd = np.array([0.0,0.0])

def getKey():
    tty.setraw(sys.stdin.fileno())
    select.select([sys.stdin], [], [], 0)
    key = sys.stdin.read(1)
    settings = termios.tcgetattr(sys.stdin)
    termios.tcsetattr(sys.stdin, termios.TCSADRAIN, settings)
    return key


def run_keyboard():
    global cmd
    settings = termios.tcgetattr(sys.stdin)
    left = 0
    right = 0
    timeout = (False,False)
    timeout_s = 10000000
    left_t = 0  # How long left has been unpressed
    right_t = 0 # How long right has been unpressed
    t1 = time.time()
    speed = np.array([1.0,1.0])
    unpressed = (1,1)
    try:
        while(1):
            key = getKey()
            if key in moveBindings.keys():
                left = moveBindings[key][0]
                right = moveBindings[key][1]
            elif key in speedBindings.keys():
                speed[0] = speed[0] * speedBindings[key][0]
                speed[1] = speed[1] * speedBindings[key][1]
            else:
                left = 0
                right = 0
                if (key == '\x03'):
                    break
            # Reset if timeout
            t2 = time.time() - t1               # How much time has passed since last iteration
            unpressed = (left==0, right==0)
            left_t = unpressed[0]*left_t        # if left_t has been pressed -> reset time 
            left_t += unpressed[0]*t2           # if left_t has not been pressed -> add time
            right_t = unpressed[1]*right_t      # if right_t has been pressed -> reset time 
            right_t += unpressed[1]*t2          # if right_t has not been pressed -> add time
            timeout = (left_t > timeout_s, right_t > timeout_s)
            t1 = time.time()
            if(timeout[0]):
                print("timeout occured (l)")
                left = 0
            if(timeout[1]):
                print("timeout occured (r)")
                right = 0
            cmd[0] = speed[0]*left
            cmd[1] = speed[1]*right
            #print(left, " | ", right)
            #print(cmd[0]," | ", cmd[1])

    except Exception as e:
        print(e)

    finally:
        termios.tcsetattr(sys.stdin, termios.TCSADRAIN, settings)


if __name__=="__main__":
    brainParams = os.path.dirname(os.path.realpath(__file__)) + "../params/brain.json"
    networkParams = os.path.dirname(os.path.realpath(__file__)) + "../params/localnetwork.json"
    keyboard_thread = threading.Thread(target=run_keyboard)
    keyboard_thread.start()
    brain = Brain(brainParams, networkParams)
    while(True):
        brain.drive(cmd[0],cmd[1])
        brain.tick()
