from sys import exit
from signal import signal, SIGINT
from Brain import Brain

def handler(signal_received, frame):
    # Handle any cleanup here
    print('SIGINT or CTRL-C detected. Exiting gracefully')
    exit(0)

if __name__ == "__main__":
    signal(SIGINT, handler)
    brain = Brain("/home/debian/devastator/params/brain.json", "/home/debian/devastator/params/network.json")
    print('Running. Press CTRL-C to exit.')
    while True:
        brain.run()
    
