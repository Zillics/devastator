import json
import random
import time
from datetime import datetime
from devpy import UdpPublisher, UdpSubscriber, ControlCmd, RobotState
from flask import Flask, Response, render_template, request, jsonify
import argparse
import threading
import queue

parser = argparse.ArgumentParser(description='Publish some devastator data over UDP')
parser.add_argument('--ip', dest='ip', action='store', default='127.0.0.1', help='IP address')
parser.add_argument('--port', dest='port', action='store', default=8001, help='Port number')
args = parser.parse_args()

app = Flask(__name__)
random.seed()  # Initialize the random number generator
t0 = time.time()
stateQueue = queue.Queue()
interval_s = 0.1

def getTimedData(json_struct):
    global t0
    json_data = json.dumps(json_struct)
    return "data:{}\n\n".format(json_data)

def generate_random_data():
    global t0
    while True:
        yield getTimedData(random.random() * 100)
        time.sleep(1)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/sensor')
def sensor():
    return render_template('sensor.html')

@app.route('/state')
def state():
    return render_template('state.html')

@app.route('/data/state',methods=["POST"])
def updateState():
    global stateQueue, t0
    t = time.time() - t0
    raw_data = request.get_data().decode('utf-8')
    print(raw_data)
    data = json.loads(raw_data)
    data['time'] = t
    stateQueue.put(data)
    response = json.dumps(
        {'status': 'success'})
    return Response(response)

@app.route('/plot-data/rstate/')
def chart_data():
    global interval_s
    def getQueuedData(interval):
        global stateQueue
        while(not stateQueue.empty()):
            state = stateQueue.get()
            yield getTimedData(state)
            time.sleep(interval)
            print("not empty")
        while(stateQueue.empty()):
            # TODO: less hacky method
            yield getTimedData({'time': 0.0, 'value': 0.0})
            time.sleep(interval)
            print("empty")
    return Response(getQueuedData(interval_s), mimetype='text/event-stream')

if __name__ == '__main__':
    app.run(host=args.ip,port=args.port,debug=True, threaded=True)
