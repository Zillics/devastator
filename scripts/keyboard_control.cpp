#include <iostream>
#include <string>
#include <ncurses.h>
#include <thread>
#include <atomic>
#include <chrono>
#include <boost/program_options.hpp>
#include "rc/Controller.hpp"
#include "Logger.hpp"

/** keyboard_control.cpp -- Controls motors with keyboard */

#define DEBUG 0
#define ctrl(x)           ((x) & 0x1f)


using namespace std::literals::chrono_literals;
using namespace std::chrono;

std::atomic<bool> quit(false);
std::atomic<double> leftTrack(0.0);
std::atomic<double> rightTrack(0.0);
ControlCmd cmd;
static high_resolution_clock::time_point tL = high_resolution_clock::now();
static high_resolution_clock::time_point tR = high_resolution_clock::now();

void keyboard_control_asdw() {
    int c;
    initscr(); // Determine terminal type, initialize
    keypad(stdscr, TRUE);
    cbreak();
    noecho();
    nonl();
    printw("Keyboard control\na: left, d: right, w: forward, s: backward, q: quit\n");
    while(!quit) {
        c = getch();
        switch (c) {
            case 'a':
                leftTrack = 1.0;
                rightTrack = 0.0;
                break;
            case 's':
                leftTrack = 0.0;
                rightTrack = 0.0;
                break;
            case 'd':
                leftTrack = 0.0;
                rightTrack = 1.0;
                break;
            case 'w':
                leftTrack = 1.0;
                rightTrack = 1.0;
                break;
            case ctrl('c'):
                printw("\nkey: ctrl c");
                quit = true;
                break;
            case 'q':
                quit = true;
                break;
            default:
                leftTrack = 0.0;
                rightTrack = 0.0;
                printw("\nkeyname: %d = %s\n", c, keyname(c));
                break;
        }
    }
    endwin();
}

void keyboard_control() {
    int c;
    initscr(); // Determine terminal type, initialize
    keypad(stdscr, TRUE);
    cbreak();
    noecho();
    nonl();
    printw("Keyboard control\nw: left forward, d: left stop, v: left backward \nn: right backward, k: right stop, p: right forward \n u: forward, b: backward, q: quit\n");
    while(!quit) {
        c = getch();
        switch (c) {
            case 'w':
                #if DEBUG
                printw("LEFT FORWARD\n");
                #endif
                leftTrack = 1.0;
                tL = high_resolution_clock::now();
                break;
            case 'd':
                #if DEBUG
                printw("LEFT STOP\n");
                #endif
                leftTrack = 0.0;
                tL = high_resolution_clock::now();
                break;
            case 'v':
                #if DEBUG
                printw("LEFT BACKWARD\n");
                #endif
                leftTrack = -1.0;
                tL = high_resolution_clock::now();
                break;
            case 'p':
                #if DEBUG
                printw("RIGHT FORWARD\n");
                #endif
                rightTrack = 1.0;
                tR = high_resolution_clock::now();
                break;
            case 'k':
                #if DEBUG
                printw("RIGHT STOP\n");
                #endif
                rightTrack = 0.0;
                tR = high_resolution_clock::now();
                break;
            case 'n':
                #if DEBUG
                printw("RIGHT BACKWARD\n");
                #endif
                rightTrack = -1.0;
                tR = high_resolution_clock::now();
                break;
            case 'u':
                #if DEBUG
                printw("FORWARD\n");
                #endif
                rightTrack = 1.0;
                tR = high_resolution_clock::now();
                leftTrack = 1.0;
                tL = high_resolution_clock::now();
                break;
            case 'b':
                #if DEBUG
                printw("BACKWARD\n");
                #endif
                rightTrack = -1.0;
                tR = high_resolution_clock::now();
                leftTrack = -1.0;
                tL = high_resolution_clock::now();
                break;
            default:
                #if DEBUG
                printw("STOP\n");
                #endif
                rightTrack = 0.0;
                tR = high_resolution_clock::now();
                leftTrack = 0.0;
                tL = high_resolution_clock::now();
                break;
            case ctrl('c'):
                printw("\nkey: ctrl c");
                quit = true;
                break;
            case 'q':
                quit = true;
                break;
        }
    }
    endwin();
}

int main(int argc, char const *argv[]) {
    std::string root_dir = PROJECT_DIR;
    std::string default_controlConfig = root_dir + "/params/controller.json";
    std::string default_networkConfig = root_dir + "/params/network.json";
    std::string default_logDir = root_dir + "/build/";
    std::string controlConfigFilename, networkConfigFilename, logDir;
	bool doLogSensor = false;
    bool doLogControl = false;
    namespace po = boost::program_options;
	po::options_description description("Usage:");
	description.add_options()
		("help,h", "Display this help message")
		("config,c", po::value<std::string>()->default_value(default_controlConfig), "Controller configuration filename")
		("network,n", po::value<std::string>()->default_value(default_networkConfig), "Network configuration filename")
		("log,l", po::value<std::string>()->default_value(default_logDir), "Directory for log files")
		("sensor,s", "Log sensor data")
		("control,u", "Log control data");
	po::variables_map vm;
	po::store(po::command_line_parser(argc, argv).options(description).run(), vm);
	po::notify(vm);
	if (vm.count("help")){
		std::cout << description;
		return 0;
	}
	if (vm.count("sensor")){
        doLogSensor = true;
	}
	if (vm.count("help")){
        doLogControl = true;
	}
	controlConfigFilename = vm["config"].as<std::string>();
	networkConfigFilename = vm["network"].as<std::string>();
    logDir = vm["log"].as<std::string>();
    Controller r(controlConfigFilename, networkConfigFilename);
    CSVLogger logger(logDir);
    std::string sensorFilename = "sensor_keyboard.csv";
    if(doLogSensor) {
        logger.setSensorLog(sensorFilename);
    }
    std::string controlFilename = "control_keyboard.csv";
    if(doLogControl) {
        logger.setControlLog(controlFilename);
    }
    std::thread worker(keyboard_control);
    high_resolution_clock::time_point t;
    duration<double> timespan;
    double timeout_s = 0.5;
    while(!quit) {
        t = high_resolution_clock::now();
        timespan = duration_cast<duration<double>>(t-tL);
        if(timespan.count() > timeout_s) {
            r.setLeftTrack(0.0);
        } else {
            r.setLeftTrack(leftTrack);
        }
        timespan = duration_cast<duration<double>>(t-tR);
        if(timespan.count() > timeout_s) {
            r.setRightTrack(0.0);
        } else {
            r.setRightTrack(rightTrack);
        }
        r.tick();
        SensorState sensor = r.getSensors();
        logger.logSensors(sensor);
        ControlCmd controls = r.getControls();
        logger.logControls(controls);
        std::this_thread::sleep_for(5ms);
    }
    worker.join();
    return 0;
}
