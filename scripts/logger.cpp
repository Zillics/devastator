#include "rc/Controller.hpp"
#include "Logger.hpp"
#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <boost/program_options.hpp>

/* logger.cpp */

volatile sig_atomic_t stop;

void inthand(int signum) {
	    stop = 1;
}

int main(int argc, char **argv) {
    std::string root_dir = PROJECT_DIR;
	/* Default values */
    int ms_interval = 5;
    bool logSensor = false;
    bool logState = false;
    bool logControl = false;
    std::string
    std::string log_dir = root_dir + "/build/";
    CSVLogger logger(log_dir);

    /** Handle program options */
	namespace po = boost::program_options;
	po::options_description description("Usage:");
	description.add_options()
		("help,h", "Display this help message")
		("sensor,y", po::value<std::string>(), "Filename for sensor data")
		("state,x", po::value<std::string>(), "Filename for state data")
		("control,u", po::value<std::string>(), "Filename for control data")
		("interval,i", po::value<int>(), "Interval between sensor readings (Default: 100)");
	po::variables_map vm;
	po::store(po::command_line_parser(argc, argv).options(description).run(), vm);
	po::notify(vm);
	if (vm.count("help")){
		std::cout << description;
		return 0;
	}
	if (vm.count("sensor")){
        logSensor = true;
        std::string filename = vm["sensor"].as<std::string>();
        if(!logger.setSensorLog(filename)) {
            return 0;
        }
	}
	if (vm.count("state")){
        logState = true;
        std::string filename = vm["state"].as<std::string>();
        if(!logger.setStateLog(filename)) {
            return 0;
        }
	}
	if (vm.count("control")){
        logControl = true;
        std::string filename = vm["control"].as<std::string>();
        if(!logger.setControlLog(filename)) {
            return 0;
        }
	}
	if (vm.count("interval")){
        ms_interval = vm["interval"].as<int>();
	}

    /** Start execution */
	signal(SIGINT, inthand);
    std::string controlConfigFilename = root_dir + "/params/controller.json";
    std::string networkConfigFilename = root_dir + "/params/network.json";
    Controller r(controlConfigFilename, networkConfigFilename);
	while (!stop) {
        r.tick();
        if(logSensor) {
            SensorState sensors = r.getSensors();
            logger.logSensors(sensors);
        }
        if(logState) {
            RobotState state = r.getState();
            logger.logState(state);
        }
        if(logControl) {
            ControlCmd controls = r.getControls();
            logger.logControls(controls);
        }
		// TODO: sleep rc_usleep(1000 * ms_interval);
	}
	printf("exiting safely\n");
	system("pause");
}
