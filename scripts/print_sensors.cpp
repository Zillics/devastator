#include "rc/Controller.hpp"
#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <boost/program_options.hpp>

/* print_sensor.cpp */

volatile sig_atomic_t stop;

void inthand(int signum) {
	    stop = 1;
}

int main(int argc, char **argv) {
	/* Parse program options*/
    std::string root_dir = PROJECT_DIR;
    std::string default_controlConfig = root_dir + "/params/controller.json";
    std::string default_networkConfig = root_dir + "/params/network.json";
	std::string controlConfigFilename;
	std::string networkConfigFilename;
	namespace po = boost::program_options;
	po::options_description description("Usage:");
	description.add_options()
		("help,h", "Display this help message")
		("config,c", po::value<std::string>()->default_value(default_controlConfig), "Filename of control configuration")
		("network,n", po::value<std::string>()->default_value(default_networkConfig), "Filename of network configuration");
	po::variables_map vm;
	po::store(po::command_line_parser(argc, argv).options(description).run(), vm);
	po::notify(vm);
	if (vm.count("help")){
		std::cout << description;
		return 0;
	}
	controlConfigFilename = vm["config"].as<std::string>();
	networkConfigFilename = vm["network"].as<std::string>();
	signal(SIGINT, inthand);
	Controller r(controlConfigFilename,networkConfigFilename);
	while (!stop) {
		r.updateSensors();
		r.printSensors();
	}
	printf("exiting safely\n");
	system("pause");
}
