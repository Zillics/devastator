#include "rc/Controller.hpp"
#include <unistd.h>
#include <stdio.h>
#include <signal.h>

/* print_state.cpp */

volatile sig_atomic_t stop;

void inthand(int signum) {
	    stop = 1;
}

int main(int argc, char **argv) {
	signal(SIGINT, inthand);
    std::string root_dir = PROJECT_DIR;
    std::string controlConfigFilename = root_dir + "/params/controller.json";
    std::string networkConfigFilename = root_dir + "/params/network.json";
	Controller r(controlConfigFilename,networkConfigFilename);
	while (!stop) {
		r.tick();
		r.printState();
	}
	printf("exiting safely\n");
	system("pause");
}
