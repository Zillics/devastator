#include "rc/Controller.hpp"
#include <unistd.h>
#include <stdio.h>
#include <signal.h>
#include <boost/program_options.hpp>
#include <limits>

/* run_controller.cpp */

volatile sig_atomic_t stop;

void inthand(int signum) {
	    stop = 1;
}

int main(int argc, char **argv) {
	std::string root_dir = PROJECT_DIR;
	root_dir = root_dir + "/"
    std::string default_controlConfig = "params/controller.json";
    std::string default_networkConfig = "params/network.json";
	/* Parse program options*/
	std::string controlConfigFilename, networkConfigFilename;
	double freq_hz;
	bool useKalman;
	namespace po = boost::program_options;
	po::options_description description("Usage:");
	description.add_options()
		("help,h", "Display this help message")
		("config,c", po::value<std::string>()->default_value(default_controlConfig), "Filename of control configuration")
		("network,n", po::value<std::string>()->default_value(default_networkConfig), "Filename of network configuration")
		("freq,r", po::value<double>()->default_value(100), "Frequency of ticks in Hz")
		("kalman,k", po::value<bool>()->default_value(false), "Uses Extended Kalman filter to estimate robot state");
	po::variables_map vm;
	po::store(po::command_line_parser(argc, argv).options(description).run(), vm);
	po::notify(vm);
	if (vm.count("help")) {
		std::cout << description;
		return 0;
	}
	controlConfigFilename = root_dir + vm["config"].as<std::string>();
	networkConfigFilename = root_dir + vm["network"].as<std::string>();
	freq_hz = vm["freq"].as<double>();
	useKalman = vm["kalman"].as<bool>();
	signal(SIGINT, inthand);
	Controller r(controlConfigFilename,networkConfigFilename);
	double interval_s = 1/freq_hz;
	std::cout << "Running with tick interval " << interval_s << "\n";
	while (!stop) {
		// Run for eternity until stop
		//r.run(freq_hz,20);
		r.tick(interval_s,useKalman);
    }
	
	printf("exiting safely\n");
	system("pause");
}
