#include <stdio.h>
#include <boost/program_options.hpp>
#include <limits>
#include "Utils.hpp"
#include "mock/Simulation.hpp"
/* run_simulation.cpp */

int main(int argc, char **argv) {
	std::string root_dir = PROJECT_DIR;
	root_dir = root_dir + "/";
    std::string default_config = "params/simulation.json";
	/* Parse program options*/
	std::string config;
    int duration_s;
	namespace po = boost::program_options;
	po::options_description description("Usage:");
	description.add_options()
		("help,h", "Display this help message")
		("config,c", po::value<std::string>()->default_value(default_config), "Filename of simulation configuration")
		("duration,d", po::value<int>()->default_value(5), "Duration in seconds to run simulation for");
	po::variables_map vm;
	po::store(po::command_line_parser(argc, argv).options(description).run(), vm);
	po::notify(vm);
	if (vm.count("help")) {
		std::cout << description;
		return 0;
	}
    // Create simulation
	config = root_dir + vm["config"].as<std::string>();
    duration_s = vm["duration"].as<int>();
    // Test
    Simulation sim(config);
    std::cout << "Running simulation for " << duration_s << " seconds\n";
    sim.run(duration_s);
	printf("exiting safely\n");
	system("pause");
}
