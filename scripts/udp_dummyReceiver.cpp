#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <boost/bind.hpp>
#include <thread>
#include <chrono>
#include <iostream>
#include <memory>
#include <boost/program_options.hpp>
#include "NetUtils.hpp"
#include "Datatypes.hpp"

/* udp_sender.cpp */

#define DELAY 1000
volatile sig_atomic_t stop;
using boost::asio::ip::udp;
using boost::asio::ip::address;

void inthand(int signum) {
	    stop = 1;
}

void receiveCmd(UdpSubscriber& sub, ControlCmd& data) {
    signal(SIGINT, inthand);
    sub.start();
    while(!stop) {
        if(sub.hasReceived()) {
            std::cout << "Data packet received!\n";
            sub.read(data);
            data.print();
        }
    }
    std::cout << "Exiting...\n";
}

void receiveSensor(UdpSubscriber& sub, SensorState& data) {
    signal(SIGINT, inthand);
    sub.start();
    while(!stop) {
        if(sub.hasReceived()) {
            std::cout << "Data packet received!\n";
            sub.read(data);
            data.print();
        }
    }
    std::cout << "Exiting...\n";
}

void receiveRobotState(UdpSubscriber& sub, RobotState& data) {
    signal(SIGINT, inthand);
    sub.start();
    while(!stop) {
        if(sub.hasReceived()) {
            std::cout << "Data packet received!\n";
            sub.read(data);
            data.print();
        }
    }
    std::cout << "Exiting...\n";
}

void run(char type, UdpSubscriber& sub) {
    switch(type) {
        case 'c': {
            ControlCmd cmd;
            receiveCmd(sub,cmd);
            break;
        }
        case 's': {
            SensorState sensor;
            receiveSensor(sub,sensor);
            break;
        }
        case 'r': {
            RobotState state;
            receiveRobotState(sub,state);
            break;
        }
        default: {
            ControlCmd cmd2;
            receiveCmd(sub,cmd2);
            break;
        }
    }
}

int main(int argc, char *argv[]) {
    std::string ip;
    std::string filename = "";
    char type;
    int port;
	namespace po = boost::program_options;
	po::options_description description("Usage:");
	description.add_options()
		("help,h", "Display this help message")
		("ip,i", po::value<std::string>()->default_value("127.0.0.1"), "IP address")
		("port,p", po::value<int>()->default_value(6666), "Port")
		("file,f", po::value<std::string>()->default_value(""), "Configuration filename (overrides ip and port)")
        ("datatype,t", po::value<char>()->default_value('c'), "c: ControlCmd, s: SensorState, r: RobotState");
	po::variables_map vm;
	po::store(po::command_line_parser(argc, argv).options(description).run(), vm);
	po::notify(vm);
	if (vm.count("help")){
		std::cout << description;
		return 0;
	}
	ip = vm["ip"].as<std::string>();
	port = vm["port"].as<int>();
    type = vm["datatype"].as<char>();
	filename = vm["file"].as<std::string>();
    if(filename != "") {
        std::string root_dir = PROJECT_DIR;
        filename = root_dir + "/" + filename;
        UdpSubscriber sub(filename);
        run(type,sub);
    } else {
        UdpSubscriber sub(ip,port);
        run(type,sub);
    }

    std::cout << "Receiving ControlCmd.....\n";
    return 0;
}
