#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <boost/bind.hpp>
#include <thread>
#include <chrono>
#include <iostream>
#include <boost/program_options.hpp>
#include "NetUtils.hpp"
#include "Datatypes.hpp"

/* udp_sender.cpp */

#define DELAY 1000
volatile sig_atomic_t stop;
using boost::asio::ip::udp;
using boost::asio::ip::address;

void inthand(int signum) {
	    stop = 1;
}

void publishCmd(UdpPublisher& pub, int interval) {
    signal(SIGINT, inthand);
    std::cout << "Publishing ControlCmd with interval " << interval << " ms.....\n";
    ControlCmd cmd;
    cmd.u(0) = 0.4;
    cmd.u(1) = 0.6;
    while(!stop) { 
        pub.publish(cmd);
        std::this_thread::sleep_for(std::chrono::milliseconds(interval));
    }
    std::cout << "Exiting...\n";
}

void publishSensor(UdpPublisher& pub, int interval) {
    signal(SIGINT, inthand);
    std::cout << "Publishing SensorState with interval " << interval << " ms.....\n";
	SensorState sensor;
    while(!stop) { 
        pub.publish(sensor);
        std::this_thread::sleep_for(std::chrono::milliseconds(interval));
    }
    std::cout << "Exiting...\n";
}

void publishRobotState(UdpPublisher& pub, int interval) {
    signal(SIGINT, inthand);
    std::cout << "Publishing SensorState with interval " << interval << " ms.....\n";
	RobotState state;
    while(!stop) { 
        pub.publish(state);
        std::this_thread::sleep_for(std::chrono::milliseconds(interval));
    }
    std::cout << "Exiting...\n";
}

void run(char type, UdpPublisher& pub , int interval) {
    switch(type) {
        case 'c': {
            publishCmd(pub,interval);
            break;
        }
        case 's': {
            publishSensor(pub,interval);
            break;
        }
        case 'r': {
            publishRobotState(pub,interval);
            break;
        }
        default: {
            publishCmd(pub,interval);
            break;
        }
    }
}

int main(int argc, char *argv[]) {
    std::string ip, filename;
    int port, interval;
	char type;
	namespace po = boost::program_options;
	po::options_description description("Usage:");
	description.add_options()
		("help,h", "Display this help message")
		("ip,i", po::value<std::string>()->default_value("127.0.0.1"), "IP address")
		("port,p", po::value<int>()->default_value(6666), "Port")
		("file,f", po::value<std::string>()->default_value(""), "Configuration filename (overrides ip and port)")
		("interval,d", po::value<int>()->default_value(DELAY), "Publishing interval in ms.")
        ("datatype,t", po::value<char>()->default_value('c'), "c: ControlCmd, s: SensorState");
	po::variables_map vm;
	po::store(po::command_line_parser(argc, argv).options(description).run(), vm);
	po::notify(vm);
	if (vm.count("help")){
		std::cout << description;
		return 0;
	}
	ip = vm["ip"].as<std::string>();
	port = vm["port"].as<int>();
	filename = vm["file"].as<std::string>();
	interval = vm["interval"].as<int>();
	type = vm["datatype"].as<char>();

    if(filename != "") {
        UdpPublisher pub(filename);
		run(type,pub,interval);
    } else {
        UdpPublisher pub(ip,port,"dummyPublisher");
		run(type,pub,interval);
    }
}
