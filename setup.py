import os
import re
import sys
import platform
import subprocess

from setuptools import setup, Extension
from setuptools.command.build_ext import build_ext
from distutils.version import LooseVersion

from subprocess import CalledProcessError

class CMakeExtension(Extension):
    def __init__(self, name, sourcedir=''):
        Extension.__init__(self, name, sources=[])
        self.sourcedir = os.path.abspath(sourcedir)


class CMakeBuild(build_ext):
    def run(self):
        try:
            out = subprocess.check_output(['C:/Program Files/CMake/bin/cmake', '--version'])
        except OSError:
            raise RuntimeError("CMake must be installed to build the following extensions: " +
                               ", ".join(e.name for e in self.extensions))

        if platform.system() == "Windows":
            cmake_version = LooseVersion(re.search(r'version\s*([\d.]+)', out.decode()).group(1))
            if cmake_version < '3.17.1':
                raise RuntimeError("CMake >= 3.17.1 is required on Windows")

        for ext in self.extensions:
            self.build_extension(ext)

    def build_extension(self, ext):
        cmake_generator = "MinGW Makefiles"
        c_compiler = "gcc"
        cxx_compiler = "g++"
        extdir = os.path.abspath(os.path.dirname(self.get_ext_fullpath(ext.name)))
        # required for auto-detection of auxiliary "native" libs
        if not extdir.endswith(os.path.sep):
            extdir += os.path.sep

        # Specify compiler used for cmake
        cmake_args = ["-G", cmake_generator ,"-D","CMAKE_C_COMPILER="+c_compiler,"-D","CMAKE_CXX_COMPILER="+cxx_compiler]

        cmake_args += ['-DCMAKE_LIBRARY_OUTPUT_DIRECTORY=' + extdir,
                      '-DPYTHON_EXECUTABLE=' + sys.executable]

        cfg = 'Debug' if self.debug else 'Release'
        build_args = ['--config', cfg]

        if platform.system() == "Windows" and not c_compiler == "gcc":
            cmake_args += ['-DCMAKE_LIBRARY_OUTPUT_DIRECTORY_{}={}'.format(cfg.upper(), extdir)]
            build_args += ['--', '/m']
        else:
            print("----------->MINGW")
            cmake_args += ['-DCMAKE_BUILD_TYPE=' + cfg]
            build_args += ['--', '-j2']

        env = os.environ.copy()
        env['CXXFLAGS'] = '{} -DVERSION_INFO=\\"{}\\"'.format(env.get('CXXFLAGS', ''),
                                                              self.distribution.get_version())
        print("BUILD_TEMP: ", self.build_temp)
        print("SOURCE_DIR: ", ext.sourcedir)
        if not os.path.exists(self.build_temp):
            os.makedirs(self.build_temp)
        for arg in cmake_args:
            print(arg)
        subprocess.check_call(['C:/Program Files/CMake/bin/cmake', ext.sourcedir] + cmake_args, cwd=self.build_temp, env=env)
        subprocess.check_call(['C:/Program Files/CMake/bin/cmake', '--build', '.'] + build_args, cwd=self.build_temp)

kwargs = dict(
    name='devpy',
    version='0.0.1',
    author='Erik Zilliacus',
    author_email='erik.zilliacus@gmail.com',
    description='Python bindings for Devastator',
    long_description='',
    ext_modules=[CMakeExtension('devpy._devpy')],
    cmdclass=dict(build_ext=CMakeBuild),
    zip_safe=False,
    packages=['devpy']
)

# likely there are more exceptions, take a look at yarl example
try:
    setup(**kwargs)        
except CalledProcessError:
    print('Failed to build extension!')
    del kwargs['ext_modules']
    setup(**kwargs)