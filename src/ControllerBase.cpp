#include "ControllerBase.hpp"
#include <assert.h>
#include <chrono>
#include <ctime>

using namespace std::chrono;

ControllerBase::ControllerBase(IMU& imu, DCMotor& leftMotor, DCMotor& rightMotor, const std::string& controlConfigFilename, const std::string& netConfigFilename) 
							: ControllerBase(imu, leftMotor, rightMotor, openJson(controlConfigFilename), openJson(netConfigFilename))
{
}

ControllerBase::ControllerBase(IMU& imu, DCMotor& leftMotor, DCMotor& rightMotor, json controlParams, json netParams)
									: m_imu(imu)
									, m_leftMotor(leftMotor)
									, m_rightMotor(rightMotor)
									, m_clock()
									, m_controls(0.0,0.0)
									, m_sensor(), m_state() 
									, m_controlSub(netParams["control_udp"])
									, m_statePub(netParams["state_udp"])
									, m_sensorPub(netParams["sensor_udp"])
									, m_imuCalibDuration(controlParams.value("auto_calibration_time_ms",-1.0))
									, m_imuCalibNumSamples(controlParams.value("auto_calibration_num_samples",-1.0))
									, m_ekf(controlParams["EKF"])
{
	assert(m_imuCalibDuration > 0.0);
	assert(m_imuCalibNumSamples > 0.0);
	// Maximum input voltage from H-bridge: 6.0. Allowed maximum voltage for fan: 5.0
	//double fanLimit = 5.0 / 6.0;
	// Balance motors for straight movement
	try {
		m_leftMotor.setLimit(controlParams["motor_balancing"]["left"].get<double>());
		m_rightMotor.setLimit(controlParams["motor_balancing"]["right"].get<double>());
	} catch (std::exception& e) {
		std::cout << "[ControllerBase] Could not set motor limits: " << e.what() << '\n';
	}
	// All required initialization
	setup();
}

void ControllerBase::setup() {
	m_controlSub.start();
	m_imu.setup();
	std::cout << "[ControllerBase] Autocalibrating IMU for " << m_imuCalibDuration << " ms.....\n";
	m_imu.autoCalibrate(m_imuCalibDuration,m_imuCalibNumSamples);
	Eigen::VectorXd x0(6);
	// Extended kalman filter
	updateSensors();
	// Set all initial state variables as 0, except angle which is fetched from IMU
	x0.setZero();
	x0(2) = m_sensor.angle();
	m_ekf.initialize(x0);
	m_clock.tic(ekf_timer);
}

void ControllerBase::tick() {
	// 1. Fetch received UDP data
	if(m_controlSub.hasReceived()) {
		m_controlSub.read(m_controls);
		std::cout << "[ControllerBase] Got control cmd!\n";
		m_controls.print();
	}
	// 2. Fetch sensor data
	updateSensors();
	// 3. Control
	m_leftMotor.drive(m_controls.u(0));
	m_rightMotor.drive(m_controls.u(1));
	// 4. Update state
	updateState();
	// 5. Publish data
	m_statePub.publish(m_state);
	m_sensorPub.publish(m_sensor);
}

void ControllerBase::tick(double interval_s) {
	assert(interval_s > 0);
	int interval_micros = interval_s * 1e6;
	auto t2 = std::chrono::high_resolution_clock::now() + std::chrono::microseconds(interval_micros);
	tick();
	auto t1 = std::chrono::high_resolution_clock::now();
	if( t1 > t2) {
		std::cout << "[ControllerBase] WARNING: one tick took more time (" << duration_cast<milliseconds>(t1 - t2).count()  << " microseconds) than desired interval ( " << interval_s * 1e6 << "microseconds)\n";
		return;
	}
	std::this_thread::sleep_until(t2);
}

void ControllerBase::run(int freq, int duration_s) {
	uint64_t t0 = t();
	double interval_s = 1/(double)freq;
	uint64_t interval_micros = interval_s * 1e6;
	std::cout << "Running with interval " << interval_s << " for duration " << duration_s << '\n';
	while( (t() - t0) < duration_s * 1e6 ) {
		tick(interval_s);
	}
}

bool ControllerBase::hasReceived() {
	bool received = m_controlSub.hasReceived();
	if(received) {
		m_controlSub.read(m_controls);
	}
	return received;
}

void ControllerBase::updateSensors() {
	m_imu.getReadings(m_sensor.z);
	m_state.t = (double)m_clock.t_millis();
}

void ControllerBase::updateState() {
	double dt = (double)m_clock.toc(ekf_timer);
	dt = dt * 1e-6; // Convert to seconds
	m_ekf.step(m_controls.u, m_sensor.z, dt); 
	m_state.s = m_ekf.getState();
	m_state.t = (double)m_clock.t_millis();
	// Update state
	m_state.u = m_controls.u;
	m_state.z = m_sensor.z;
	m_clock.tic(ekf_timer);
}

void ControllerBase::setControl(ControlCmd& cmd) {
    m_controls = cmd;
	m_controls.t = (double)m_clock.t_millis();
}

void ControllerBase::setLeftTrack(double speed) {
    m_controls.u(0) = speed;
	m_controls.t = (double)m_clock.t_millis();
}

void ControllerBase::setRightTrack(double speed) {
    m_controls.u(1) = speed;    
	m_controls.t = (double)m_clock.t_millis();
}

void ControllerBase::setTracks(double lSpeed, double rSpeed) {
    m_controls.u(0) = lSpeed;
    m_controls.u(1) = rSpeed;
	m_controls.t = (double)m_clock.t_millis();
}
