#include "Datatypes.hpp"

static Eigen::IOFormat csvFormat(Eigen::StreamPrecision,Eigen::DontAlignCols, ", ", ", ", "", "", "", "");
static Eigen::IOFormat printFormat(Eigen::StreamPrecision,0, ", ", ", ", "", "", "", "");

void ControlCmd::print() const {
    std::cout << u.format(printFormat) << "\n";
}

void ControlCmd::log(std::ofstream& ofs) const {
    ofs << t << "," << u.format(csvFormat) << "\n";
}

void RobotState::print() const {
    std::cout << "t: " << t << "," << "s: " << s.format(printFormat) << ",u: " << u.format(printFormat) << "z: " << z.format(printFormat) << "\n";
}

void RobotState::log(std::ofstream& ofs) const {
    ofs << std::fixed << t << "," << s.format(csvFormat) << "\n";
}

void SensorState::print() const { 
    std::cout << "sensor: " << z.format(printFormat) << "\n"; 
}

void SensorState::log(std::ofstream& ofs) const {
    ofs << t << "," << z.format(csvFormat) << "\n";
}
