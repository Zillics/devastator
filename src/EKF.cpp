#include "EKF.hpp"

static Eigen::IOFormat printFormat(Eigen::StreamPrecision,0, ", ", ", ", "", "", "", "");

Eigen::VectorXd Model::prediction(Eigen::VectorXd& prevState, Eigen::VectorXd& control, double dt) {
    double x = prevState(0) + dt*prevState(3);
    double y = prevState(1) + dt*prevState(4);
    double theta = prevState(2) + dt*prevState(5);
    double x_vel = prevState(3) + ((control(0) + control(1)) / 2) * sin(prevState(2));
    double y_vel = prevState(4) + ((control(0) + control(1)) / 2) * cos(prevState(2));
    double omega = prevState(5) + (control(0) - control(1)) / m_r;
    Eigen::Matrix<double, 6, 1> x_hat;
    x_hat << x, y, theta, x_vel, y_vel, omega; 
    return x_hat;
}

Eigen::VectorXd Model::innovation(Eigen::VectorXd& observation, Eigen::VectorXd& prediction) {
    double total_angle = m_sensorRotation + prediction(2);
    double v_x = prediction(3)*cos(total_angle) + prediction(4)*sin(total_angle);
    double v_y = prediction(3)*sin(total_angle) + prediction(4)*cos(total_angle);
    double theta = prediction(2);
    double omega = prediction(5);
    Eigen::Vector4d h;
    h << v_x, v_y, theta, omega;
    Eigen::Vector4d y_hat = observation - h;
    return y_hat;
}

Eigen::MatrixXd Model::stateJacobian(Eigen::VectorXd& x, Eigen::VectorXd& u, double dt) {    
    int rows = m_xN;
    int cols = m_xN;
    Eigen::MatrixXd F(rows,cols);
    double j43 = 0.5*(u(0)+u(1))*cos(x(2));
    double j53 = -0.5*(u(0)+u(1))*sin(x(2));
    F <<    1,  0,  0,      dt, 0,  0,  
            0,  1,  0,      0,  dt, 0,  
            0,  0,  1,      0,  0,  dt, 
            0,  0,  j43,    1,  0,  0,  
            0,  0,  j53,    0,  1,  0,  
            0,  0,  0,      0,  0,  1;
    return F;
}

Eigen::MatrixXd Model::observationJacobian(Eigen::VectorXd& x, double dt) {
    int rows = m_zN;
    int cols = m_xN;
    Eigen::MatrixXd H(rows,cols);
    double theta2 = m_sensorRotation + x(2);
    double j13 = -sin(theta2)*x(3) + cos(theta2)*x(4);
    double j23 = -sin(theta2)*x(4) + cos(theta2)*x(3);
    H <<    0,  0,  j13,    cos(theta2),    sin(theta2),    0,
            0,  0,  j23,    sin(theta2),    cos(theta2),    0,
            0,  0,  1,      0,              0,              0,
            0,  0,  0,      0,              0,              1;
    return H;
}

EKF::EKF(json modelParams, const std::vector<double>& q, const std::vector<double>& r, bool printSteps, bool printDebug) : m_model(modelParams)
                                                                    , m_xN(m_model.getStateDim())
                                                                    , m_uN(m_model.getControlDim())
                                                                    , m_zN(m_model.getObservationDim())
                                                                    , m_x(m_xN)
                                                                    , m_P(m_xN,m_xN)
                                                                    , m_Q(m_xN,m_xN)
                                                                    , m_R(m_zN, m_zN)
                                                                    , m_I(m_xN,m_xN)
                                                                    , m_printSteps(printSteps)
                                                                    , m_printDebug(printDebug)
                                                                    , m_iStep(0)
{
    assert(q.size() == m_xN);
    assert(r.size() == m_zN);
    for(auto i = 0; i < q.size(); i++) {
        m_Q(i,i) = q[i];
    }
    for(auto i = 0; i < r.size(); i++) {
        m_R(i,i) = r[i];
    }
    m_I.setIdentity();
    std::cout   << "[EKF] Matrices: \n";
    std::cout   << "x: " << m_x << "\n"; 
    std::cout   << "P:\n " << m_P << "\n";
    std::cout   << "Q:\n " << m_Q << "\n";
    std::cout   << "R:\n " << m_R << "\n";
    std::cout   << "I:\n " << m_I << "\n";
}

void EKF::initialize(Eigen::VectorXd& x0) {
    assert(x0.rows() == m_xN);
    m_x = x0;
    m_P.setIdentity();
    std::cout << "[EKF] Initializing...\n";
    std::cout << "P: " << m_P << "\n";
    std::cout << "x: " << m_x << "\n";
    m_iStep = 0;
}

void EKF::step(Eigen::VectorXd u, Eigen::VectorXd z, double dt) {
    // Prediction
    Eigen::VectorXd x_hat = m_model.prediction(m_x,u,dt);
    Eigen::MatrixXd F = m_model.stateJacobian(m_x, u, dt);
    Eigen::MatrixXd P_hat = F * m_P * F.transpose() + m_Q;
    // Update
    Eigen::VectorXd y_hat = m_model.innovation(z,x_hat);
    Eigen::MatrixXd H = m_model.observationJacobian(x_hat,dt);
    Eigen::MatrixXd S = H * P_hat * H.transpose() + m_R;
    Eigen::MatrixXd K = P_hat * H.transpose() * S.inverse();
    m_x = x_hat + K * y_hat;
    m_P = (m_I - K*H) * P_hat;
    if(m_printSteps) {
        printStep(m_x,m_P,u,z,dt);
    }
    if(m_printDebug) {
        std::cout << "state: " << m_x(0) << ", " << m_x(1) << '\n';
    }
    m_iStep += 1;
}

void EKF::printStep(Eigen::VectorXd& x, Eigen::MatrixXd& P,Eigen::VectorXd& u, Eigen::VectorXd& z, double dt) const {
    std::cout   << "*************** Step " << m_iStep << " ***************\n"
                << "Predicted state x: " << x.transpose() << '\n'
                << "Covariance matrix P: \n" << P << '\n'
                //<< "Control u: " << u.format(printFormat) << '\n'
                //<< "Observation z: " << z.format(printFormat) << '\n'
                << "Delta t: " << dt << '\n';
}
