#include "Logger.hpp"

Log::Log(std::string& _filename) {
    open(_filename);
}

Log::~Log() {
    if(filestream.is_open()) {
        std::cout << "[Log] Closing filestream for " << filename << "\n";
        filestream.close();
    }
}

bool Log::open(std::string& _filename) {
    filename = _filename;
    filestream.open(filename, std::ios_base::out | std::ios_base::trunc);
    if(filestream.is_open()) {
        std::cout << "[Log] Opening file " << filename << " successfully!\n";
        return true;
    } else {
        std::cout << "[Log] Opening file " << filename << " failed!\n";
        return false;
    }
}

CSVLogger::CSVLogger(std::string root_dir) : m_root_dir(root_dir), m_controlLog(), m_sensorLog(), m_stateLog() {}

CSVLogger::~CSVLogger() { }

bool CSVLogger::setControlLog(std::string& filename) {
    std::string absolutePath = m_root_dir + filename;
    std::cout << "[CSVLogger] Setting up control log in file " << absolutePath << "\n";
    return m_controlLog.open(absolutePath);
}

bool CSVLogger::setSensorLog(std::string& filename) {
    std::string absolutePath = m_root_dir + filename;
    std::cout << "[CSVLogger] Setting up sensor log in file " << absolutePath << "\n";
    return m_sensorLog.open(absolutePath);
}

bool CSVLogger::setStateLog(std::string& filename) {
    std::string absolutePath = m_root_dir + filename;
    std::cout << "[CSVLogger] Setting up state log in file " << absolutePath << "\n";
    return m_stateLog.open(absolutePath);
}

bool CSVLogger::logControls(ControlCmd& cmd) {
    return _log(cmd,m_controlLog);
}

bool CSVLogger::logSensors(SensorState& sensor) {
    return _log(sensor,m_sensorLog);
}

bool CSVLogger::logState(RobotState& state) {
    return _log(state,m_stateLog);
}

bool CSVLogger::_log(DataType& data, Log& logObj) {
    if(!logObj.filestream.is_open()) {
        std::cout << "[CSVLogger] Log object for " << logObj.filename << " is not open! Log failed.\n";
        return false;
    }
    data.log(logObj.filestream);
    return true;
}
