#include "NetUtils.hpp"
#include <exception>

using namespace std::literals::chrono_literals;
using namespace std::chrono;
using boost::asio::ip::udp;
using boost::asio::ip::address;
using json = nlohmann::json;

UdpPublisher::UdpPublisher(const std::string& address, int port, const std::string& name)
                                        : m_name(name)
                                        , m_address(address)
                                        , m_port(port)
                                        , m_service()
                                        , m_socket(m_service)
{
    m_endpoint.address(address::from_string(m_address));
    m_endpoint.port(m_port);
    std::cout << "[UdpPublisher] Setting up publisher " << m_name << " at address " << m_endpoint.address().to_string() << " on port " << m_endpoint.port() << '\n';
    m_socket.open(udp::v4());
}

UdpPublisher::UdpPublisher(const std::string& filename) : UdpPublisher(openJson(filename)) {
}

UdpPublisher::UdpPublisher(json params) : UdpPublisher(params.value("ip","NO_IP"),params.value("port",0),params.value("name","NO_NAME")) {}

UdpPublisher::~UdpPublisher() {
    m_socket.close();
}

void UdpPublisher::publish(const std::string& data) {
    boost::system::error_code err;
    m_socket.send_to(boost::asio::buffer(data), m_endpoint, 0, err);
}

void UdpPublisher::publish(ControlCmd& data) {
    boost::system::error_code err;
    std::stringstream ss;
    boost::archive::text_oarchive oar(ss);
    oar & data;
    std::string data_str = ss.str();
    m_socket.send_to(boost::asio::buffer(data_str), m_endpoint, 0, err);
}

void UdpPublisher::publish(RobotState& data) {
    boost::system::error_code err;
    std::stringstream ss;
    boost::archive::text_oarchive oar(ss);
    oar & data;
    std::string data_str = ss.str();
    m_socket.send_to(boost::asio::buffer(data_str), m_endpoint, 0, err);
}

void UdpPublisher::publish(SensorState& data){
    boost::system::error_code err;
    std::stringstream ss;
    boost::archive::text_oarchive oar(ss);
    oar & data;
    std::string data_str = ss.str();
    m_socket.send_to(boost::asio::buffer(data_str), m_endpoint, 0, err);
}

UdpSubscriber::UdpSubscriber(const std::string& address, int port, const std::string& name) 
                                          : m_name(name)
                                          , m_address(address)
                                          , m_port(port)
                                          , m_service()
                                          , m_socket(m_service)
                                          , m_receiverEndpoint(address::from_string(m_address),m_port)
                                          , m_senderEndpoint()
                                          , m_buffer()
                                          , m_nBytes(0)
                                          , m_received(false)
                                          , m_ioState(OFF)
                                          , m_shutDown(false)
                                          , m_ioThread(&UdpSubscriber::runIO,this)
                                          , m_globalExceptionPtr(nullptr)
                                          , m_clock()
{
    std::cout << "[UdpSubscriber] Setting up subscriber " << m_name << " at address " << m_receiverEndpoint.address().to_string() << " on port " << m_receiverEndpoint.port() << '\n';
    m_socket.open(udp::v4());
    boost::asio::socket_base::reuse_address reuse(true);
    m_socket.set_option(reuse);
    try {
        m_socket.bind(m_receiverEndpoint);
    } catch (const std::exception &ex) {
        std::cout << "[UdpSubscriber] Failed to bind socket to " << m_receiverEndpoint.address().to_string() << '\n';
        throw;
    }
}

UdpSubscriber::UdpSubscriber(const std::string& filename) : UdpSubscriber(openJson(filename)) {}

UdpSubscriber::UdpSubscriber(json params) : UdpSubscriber(params.value("ip","NO_IP"), params.value("port",0), params.value("name","NO_NAME")) {}

UdpSubscriber::~UdpSubscriber() {
    m_shutDown = true;
    m_service.stop();
    bool timeout = false;
    uint64_t timeout_ms = 1000;
    m_clock.tic(0);
    while(!m_ioThread.joinable() && !timeout) {
        timeout = m_clock.toc(0) > timeout_ms * 1e3;
    }
    if(m_ioThread.joinable()) {
        m_ioThread.join();
    } else {
        std::cout << "[UdpSubscriber] could not join thread. Hopefully nothing bad will happen...\n";
    }
    if (m_globalExceptionPtr) {
        try {
            std::rethrow_exception(m_globalExceptionPtr);
        } catch (const std::exception &ex) {
            std::cout << "[UdpSubscriber] Thread exited with exception: " << ex.what() << "\n";
        }
    }
    std::cout << "[UdpSubscriber] Closing socket...\n";
    m_socket.close();
}

void UdpSubscriber::runIO() {
    while(!m_shutDown) {
        try {
            switch (m_ioState) {
                case ON:
                    #if DEBUG_UDP
                    std::cout << "[runIO] ON\n";
                    #endif
                    if(m_service.stopped()) {
                        m_service.reset();
                    }
                    // Should run until service is stopped OR error occurs in receive handler
                    m_service.run();
                    break;
                case OFF:
                    #if DEBUG_UDP
                    std::cout << "[runIO] OFF\n";
                    #endif
                    break;
            }
        } catch (...){
            //Set the global exception pointer in case of an exception
            m_globalExceptionPtr = std::current_exception();
        }
    }
}

void UdpSubscriber::start() {
    if(m_ioState == OFF) {
        m_socket.async_receive_from(boost::asio::buffer(m_buffer),
                                m_senderEndpoint,
                                boost::bind(&UdpSubscriber::receiveHandler, 
                                            this, 
                                            boost::asio::placeholders::error,
                                            boost::asio::placeholders::bytes_transferred));
    }
    m_ioState = ON;
}

void UdpSubscriber::stop() {
    m_ioState = OFF;
    if(!m_service.stopped()){
        m_service.stop();
    }
}

void UdpSubscriber::receiveHandler(const boost::system::error_code& error, size_t bytes_transferred) {
    #if DEBUG_UDP
    std::cout << "[UdpSubscriber] Received: " << std::string(m_buffer.begin(), m_buffer.begin()+bytes_transferred) << "' (" << error.message() << ")\n";
    #endif
    if (error) {
        #if DEBUG_UDP
        std::cout << "Receive failed: " << error.message() << '\n';
        #endif
        return;
    }
    m_nBytes = bytes_transferred;
    m_received = true;
    // Continue receiving
    m_socket.async_receive_from(boost::asio::buffer(m_buffer),
                                m_senderEndpoint,
                                boost::bind(&UdpSubscriber::receiveHandler, 
                                            this, 
                                            boost::asio::placeholders::error,
                                            boost::asio::placeholders::bytes_transferred));
}

bool UdpSubscriber::read(ControlCmd& data) {
    if(m_nBytes <= 0) {
        std::cout << "[UdpSubscriber] Reading from " << m_name << " failed! No bytes received yet\n";
        return false;
    }
    m_received = false;
    try {
        std::stringstream ss;
        std::string dataString;
        std::copy(m_buffer.begin(), m_buffer.begin()+m_nBytes, std::back_inserter(dataString));
        ss.str(dataString);
        boost::archive::text_iarchive iar(ss);
        iar & data;

    } catch(boost::archive::archive_exception& e) {
        std::cout << "[UdpSubscriber] Reading from " << m_name << " failed! Non-matching data format?\n";
        return false;
    } catch(std::exception& e) {
        std::cout << "[UdpSubscriber] Reading from " << m_name << " failed! Problem: " << e.what() << "\n";
        return false;
    }
    return true;
}

bool UdpSubscriber::read(SensorState& data) {
    if(m_nBytes <= 0) {
        std::cout << "[UdpSubscriber] Reading from " << m_name << " failed! No bytes received yet\n";
        return false;
    }
    m_received = false;
    try {
        std::stringstream ss;
        std::string dataString;
        std::copy(m_buffer.begin(), m_buffer.begin()+m_nBytes, std::back_inserter(dataString));
        ss.str(dataString);
        boost::archive::text_iarchive iar(ss);
        iar & data;
    } catch(boost::archive::archive_exception& e) { 
        std::cout << "[UdpSubscriber] Reading from " << m_name << " failed! Non-matching data format?\n";
        return false;
    } catch(std::exception& e) {
        std::cout << "[UdpSubscriber] Reading from " << m_name << " failed! Problem: " << e.what() << "\n";
        return false;
    }
    return true;
}

bool UdpSubscriber::read(RobotState& data) {
    if(m_nBytes <= 0) {
        std::cout << "[UdpSubscriber] Reading from " << m_name << " failed! No bytes received yet\n";
        return false;
    }
    m_received = false;
    try {
        std::stringstream ss;
        std::string dataString;
        std::copy(m_buffer.begin(), m_buffer.begin()+m_nBytes, std::back_inserter(dataString));
        ss.str(dataString);
        boost::archive::text_iarchive iar(ss);
        iar & data;
    } catch(boost::archive::archive_exception& e) {
        std::cout << "[UdpSubscriber] Reading from " << m_name << " failed! Non-matching data format?\n";
        return false;
    } catch(std::exception& e) {
        std::cout << "[UdpSubscriber] Reading from " << m_name << " failed! Problem: " << e.what() << "\n";
        return false;
    }
    return true;
}
