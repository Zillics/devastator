#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include <string>
#include "EigenConfig.hpp"
#include "Datatypes.hpp"
#include "NetUtils.hpp"
#include <fstream>
#include <iostream>

namespace py = pybind11;

PYBIND11_MODULE(_devpy,m)
{
    class PyDataType : public DataType {
    public:
        /* Inherit the constructors */
        using DataType::DataType;
        /* Trampoline (need one for each virtual function) */
        void print() const override {
            PYBIND11_OVERLOAD_PURE(
                void,           /* Return type */
                DataType,      /* Parent class */
                print          /* Name of function in C++ (must match Python name) */
            );
        }
        void log(std::ofstream& ofs) const override {
            PYBIND11_OVERLOAD_PURE(
                void,           /* Return type */
                DataType,      /* Parent class */
                log           /* Name of function in C++ (must match Python name) */
            );
        }
        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    };
    
    py::class_<DataType,PyDataType>(m,"DataType",py::dynamic_attr())
        .def(py::init<>())
        .def("print",&DataType::print);

    py::class_<ControlCmd, DataType>(m,"ControlCmd",py::dynamic_attr())
        .def(py::init<>())
        .def(py::init<double,double>())
        .def("print", &ControlCmd::print)
        .def_readwrite("u",&ControlCmd::u);

    py::class_<RobotState, DataType>(m,"RobotState",py::dynamic_attr())
        .def(py::init<>())
        .def_readwrite("s",&RobotState::s)
        .def_readwrite("u",&RobotState::u)
        .def_readwrite("z",&RobotState::z)
        .def_readwrite("t",&RobotState::t)
        .def("print", &RobotState::print);

    py::class_<SensorState, DataType>(m,"SensorState",py::dynamic_attr())
        .def(py::init<>())
        .def("print", &SensorState::print)
        .def_readwrite("z",&SensorState::z)
        .def_readwrite("t",&SensorState::t);

    py::class_<UdpPublisher>(m,"UdpPublisher")
        .def(py::init<const std::string&>())
        .def(py::init<const std::string&,int,const std::string&>())
        .def("publish", (void (UdpPublisher::*)(ControlCmd&)) &UdpPublisher::publish)
        .def("publish", (void (UdpPublisher::*)(RobotState&)) &UdpPublisher::publish)
        .def("publish", (void (UdpPublisher::*)(SensorState&)) &UdpPublisher::publish);
    
    py::class_<UdpSubscriber>(m,"UdpSubscriber")
        .def(py::init<const std::string&>())
        .def(py::init<const std::string&,int,const std::string&>())
    	.def("start", &UdpSubscriber::start)
        .def("stop", &UdpSubscriber::stop)
        .def("hasReceived", &UdpSubscriber::hasReceived)
        .def("readCmd", (bool (UdpSubscriber::*)(ControlCmd&)) &UdpSubscriber::read)
        .def("readSensor", (bool (UdpSubscriber::*)(SensorState&)) &UdpSubscriber::read)
        .def("readState", (bool (UdpSubscriber::*)(RobotState&)) &UdpSubscriber::read);
}
