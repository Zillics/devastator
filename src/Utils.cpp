#include "Utils.hpp"

using json = nlohmann::json;

json openJson(const std::string& filename) {
	json params;
	std::ifstream ifs(filename);
	if (!ifs.is_open()) {
		throw std::invalid_argument("File " + filename + " could not be opened. File exists?");
	}
	ifs >> params;
	return params;
}

double degToRad(int deg) {
    double rad = ((double)deg/360.0) * (2*PI);    
	std::cout << "[degToRad]  deg: " << deg << ", rad: " << rad << "\n";
    return rad;
}

Eigen::Matrix3d rot_x(double rad_rot) {
    Eigen::Matrix3d rot;
    rot <<  1,0,0,
			0,cos(rad_rot), -sin(rad_rot),
            0,sin(rad_rot), cos(rad_rot);
    return rot;
}

Eigen::Matrix3d rot_y(double rad_rot) {
    Eigen::Matrix3d rot;
    rot <<  cos(rad_rot),0,sin(rad_rot),
			0,1,0,
            -sin(rad_rot),0,cos(rad_rot);
    return rot;
}

Eigen::Matrix3d rot_z(double rad_rot) {
    Eigen::Matrix3d rot;
    rot <<  cos(rad_rot),-sin(rad_rot),0,
			sin(rad_rot),cos(rad_rot),0,
            0,0,1;
    return rot;
}
