#include "mock/DCMotor.hpp"

mock::DCMotor::DCMotor(std::string name, int channel, std::shared_ptr<Wheel> wheel) 
                : ::DCMotor(name)
                , m_channel(channel)
                , m_enabled(true)
                , m_limit(1.0)
                , m_wheel(wheel)
{
}

mock::DCMotor::~DCMotor() {
}

bool mock::DCMotor::brake() const {
    // TODO
    return true;
}

bool mock::DCMotor::drive(double dutyCycle) const {
    // TODO
    m_wheel->drive(dutyCycle);
    return true;
}

void mock::DCMotor::setLimit(double limit) {
    m_limit = std::min(std::max(std::abs(limit), 0.0), 1.0);
}