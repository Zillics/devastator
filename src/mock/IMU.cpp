#include "mock/IMU.hpp"

mock::IMU::IMU(json params, std::shared_ptr<TankRobot> robot) 	
					: ::IMU(params.value("sample_rate",-1))
					, m_data()
					, m_mean(Eigen::Vector4d::Zero())
					, m_bias(Eigen::Vector4d::Zero())
					, m_accelNoiseStd(sqrt(params.value("accel_noise_var",-1.0)))
					, m_gyroNoiseStd(sqrt(params.value("gyro_noise_var",-1.0)))
					, m_accelXReadings(std::deque<double>(DEFAULT_NSAMPLES,0.0))
					, m_accelYReadings(std::deque<double>(DEFAULT_NSAMPLES,0.0))
					, m_thetaZReadings(std::deque<double>(DEFAULT_NSAMPLES,0.0)) // Angle
					, m_omegaZReadings(std::deque<double>(DEFAULT_NSAMPLES,0.0)) // Angular velocity
					, m_timeInstances(std::deque<double>(DEFAULT_NSAMPLES,0.0))  // Time deltas from most recent readings
					, m_clock()
					, m_dt(0.0)
					, m_dmpThread(&mock::IMU::runDmp,this)
					, m_shutDown(false)
					, m_robot(robot)
{
	assert(m_accelNoiseStd >= 0.0);
	assert(m_gyroNoiseStd >= 0.0);
}

std::normal_distribution<double> mock::IMU::m_noisePdf(0,1);
std::default_random_engine mock::IMU::m_generator(std::chrono::system_clock::now().time_since_epoch().count());

double mock::IMU::noise(double mean, double std) {
	return mean + std*m_noisePdf(m_generator);
}

void mock::IMU::setParams(json params) {
	try {
		m_sampleRate = params["sample_rate"].get<int>();
	} catch(std::exception& e) {
		std::cout << "[mock::IMU] Could not set sample_rate: " << e.what() << '\n'; 
	}
	// Admissible sample rate range : [0,200]
	m_sampleRate = std::min(std::max(0,m_sampleRate),200);
	std::cout << "[mock::IMU] Setting sample rate of " << m_sampleRate << "\n";
}


bool mock::IMU::setup() {
	// TODO
	return true;
}

void mock::IMU::getReadings(Eigen::Vector4d& readings) {
	readings = m_mean - m_bias;
	// Integrate acceleration to get velocity
	readings(0) = readings(0) * m_dt;
	readings(1) = readings(1) * m_dt;
}

void mock::IMU::autoCalibrate(double duration_ms, int numSamples) {
	uint64_t sampleInterval = (duration_ms/numSamples) * 1e3;
	Eigen::MatrixX4d readings(numSamples,4);
	int i = 0;
	m_clock.tic(calib_timer);
	while( i < numSamples ) {
		if(m_clock.toc(calib_timer) > sampleInterval) {
			readings.row(i) = m_mean;
			i++;
			m_clock.tic(calib_timer);
		}
	}
	m_bias = readings.colwise().mean();
	m_bias(2) = 0.0; // No ground truth for angle -> leave it be
}

void mock::IMU::dmpCallback(void) {
	update();
	// TODO: Less memory-intense, more efficient approach
	double newVal;
	// X acceleration
	newVal = m_data.accel[0];
	m_mean(0) = runningMean(m_accelXReadings.front(),newVal,m_mean(0),DEFAULT_NSAMPLES);
	m_accelXReadings.pop();
	m_accelXReadings.push(newVal);
	
	// Y acceleration
	newVal = m_data.accel[1];
	m_mean(1) = runningMean(m_accelYReadings.front(),newVal,m_mean(1),DEFAULT_NSAMPLES);
	m_accelYReadings.pop();
	m_accelYReadings.push(newVal);

	// Z theta
	newVal = m_data.compass_heading;
	m_mean(2) = runningMean(m_thetaZReadings.front(),newVal,m_mean(2),DEFAULT_NSAMPLES);
	m_thetaZReadings.pop();
	m_thetaZReadings.push(newVal);

	// Z omega
	newVal = m_data.gyro[2];
	m_mean(3) = runningMean(m_thetaZReadings.front(),newVal,m_mean(3),DEFAULT_NSAMPLES);
	m_thetaZReadings.pop();
	m_thetaZReadings.push(newVal);

	// dt
	// TODO : more accurate time
	newVal = (double)m_clock.t_millis();
	m_dt = newVal - m_timeInstances.front();
	m_timeInstances.pop();
	m_timeInstances.push(newVal);
}

void mock::IMU::runDmp() {
	int interval_s = (1.0/(double)m_sampleRate);
	while(!m_shutDown) {
		std::this_thread::sleep_for(std::chrono::seconds(interval_s));
		dmpCallback();
	}	
}

void mock::IMU::update() {
	// Ground truth
	const Eigen::Vector3d worldAccel = m_robot->acc();
	const Eigen::Vector3d worldAngle = m_robot->angle();
	const Eigen::Vector3d worldOmega = m_robot->omega();
	// Assumes robot front points towards y
	const double realAccelY = worldAccel[0] * sin(worldAngle[2]) + worldAccel[1] * cos(worldAngle[2]);
	m_data.accel[0] = 0 + noise(0,m_accelNoiseStd);
	m_data.accel[1] = realAccelY + noise(0,m_accelNoiseStd);
	m_data.accel[2] = 0 + noise(0,m_accelNoiseStd);
	m_data.gyro[0] = 0 + noise(0,m_gyroNoiseStd);
	m_data.gyro[1] = 0 + noise(0,m_gyroNoiseStd);
	m_data.gyro[2] = worldOmega[2] + noise(0,m_gyroNoiseStd);
}