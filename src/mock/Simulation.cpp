#include "mock/Simulation.hpp"

static uint64_t longestInterval(double freq1, double freq2) {
    uint64_t longest = ( 1 / std::min(freq1,freq2) ) * 1e6;
    return longest;
}

Simulation::Simulation(const std::string& filename) : Simulation(openJson(filename))
{
}

Simulation::Simulation(json params) 
                        : m_shutDown(false)
                        , m_runWorld(false)
                        , m_runController(false)
                        , m_worldFreq(params.value("world_freq",-1))
                        , m_controllerFreq(params.value("controller_freq",-1))
                        , m_interval_micros(longestInterval(m_worldFreq,m_controllerFreq))
                        , m_world(std::make_shared<World>(params["World"]))
                        , m_controller(params["Controller"],params["Network"], m_world)
                        , m_worldThread(&Simulation::runWorld,this,m_worldFreq)
                        , m_controllerThread(&Simulation::runController,this,m_controllerFreq)
                        , m_clock()
{
    assert(m_worldFreq > 0);
    assert(m_controllerFreq > 0);
}

Simulation::~Simulation() {
    m_runWorld = false;
    m_runController = false;
    m_shutDown = true;
    m_clock.tic(1);
    std::cout << "[Simulation] waiting " << m_interval_micros << " microseconds for threads to finish...\n";
    while(m_clock.toc(1) < m_interval_micros){ /* Wait */ }
    if(m_controllerThread.joinable()) {
        m_controllerThread.join();
    } else {
        std::cout << "[Simulation] WARN: Cannot join controller thread\n";
    }
    if(m_worldThread.joinable()) {
        m_worldThread.join();
    } else {
        std::cout << "[Simulation] WARN: Cannot join world thread\n";
    }
}

void Simulation::run(int duration_s) {
    uint64_t duration_micros = duration_s * 1e6;
    std::cout << "[Simulation] Starting simulation... Running for " << duration_micros <<" microseconds, with sleep cycles: " << m_interval_micros << " microseconds\n";
    m_clock.tic(0);
    m_runWorld = true;
    m_runController = true;
    while(m_clock.toc(0) < duration_micros){
        std::this_thread::sleep_for(std::chrono::microseconds(m_interval_micros));
    }
    m_runWorld = false;
    m_runController = false;
}

void Simulation::runWorld(double freq) {
    double interval_s = 1 / freq;
    while(!m_shutDown) {
        while(m_runWorld) {
            m_world->tick(interval_s);
        }
    }
}

void Simulation::runController(double freq) {
    double interval_s = 1 / freq;
    while(!m_shutDown) {
        while(m_runController) {
            m_controller.tick(interval_s);
        }
    }
}
