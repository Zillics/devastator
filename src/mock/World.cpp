#include "mock/World.hpp"

Wheel::Wheel(json params)   : m_maxVel(params.value("max_vel",0.0))
                            , m_maxAcc(params.value("max_acc",0.0))
{
    assert(m_maxVel > 0.0);
    assert(m_maxVel > 0.0);
    std::cout << "[Wheel] Created Wheel with parameters:\n" << params.dump() << '\n';
}

void Wheel::tick(double dt) {
    m_acc = m_dutyCycle*m_maxAcc;
    double k = std::abs(m_dutyCycle);
    m_vel = boost::algorithm::clamp(m_vel + dt*m_acc, -k*m_maxVel, k*m_maxVel);
}

State::State() : pos(Eigen::Vector3d::Zero())
        , vel(Eigen::Vector3d::Zero())
        , acc(Eigen::Vector3d::Zero())
        , angle(Eigen::Vector3d::Zero())
        , angular_v(Eigen::Vector3d::Zero())
        , angular_a(Eigen::Vector3d::Zero())
{}

void State::print() const {
    Eigen::IOFormat printFormat(Eigen::StreamPrecision,0, ", ", ", ", "", "", "", "");
    std::cout   << "pos: " << pos.format(printFormat) << '\n'
                << "vel: " << vel.format(printFormat) << '\n'
                << "acc: " << acc.format(printFormat) << '\n'
                << "angle: " << angle.format(printFormat) << '\n'
                << "angular_v: " << angular_v.format(printFormat) << '\n'
                << "angular_a: " << angular_a.format(printFormat) << '\n';
}

TankRobot::TankRobot(json params)   : m_r(params.value("radius",-1.0))
                                    , m_state()
                                    , m_leftWheel(std::make_shared<Wheel>(params["left_wheel"]))
                                    , m_rightWheel(std::make_shared<Wheel>(params["right_wheel"]))
{
    assert(m_r > 0);
    std::cout << "[TankRobot] Created TankRobot with parameters:\n" << params.dump() << '\n';
}

void TankRobot::tick(double dt) {
    updateState(dt);
    m_leftWheel->tick(dt);
    m_rightWheel->tick(dt);
}

void TankRobot::updateState(double dt) {
    Eigen::Vector3d prevVel = m_state.vel;
    Eigen::Vector3d prevOmega = m_state.angular_v;
    double vl = m_leftWheel->vel();
    double vr = m_rightWheel->vel();
    m_state.vel[0] = (1/2) * (vl + vr) * cos(m_state.angle[2]);
    m_state.vel[1] = (1/2) * (vl + vr) * sin(m_state.angle[2]);
    m_state.vel[2] = 0;         // Only 2D movement for now
    m_state.angle += prevOmega*dt;
    m_state.angular_v[0] = 0;   // Only 2D movement for now
    m_state.angular_v[1] = 0;   // Only 2D movement for now
    m_state.angular_v[2] = (1/m_r)*(vr - vl);
    m_state.acc = (m_state.vel - prevVel)/dt;
    m_state.angular_a = (m_state.angular_v - prevOmega)/dt;
}


World::World(json params)  : m_clock()
                    , m_robot(std::make_shared<TankRobot>(params["robot"]))
{
    std::cout << "[World] Creating world with parameters:\n" << params.dump() << '\n';
}

void World::tick(double dt) { 
    m_robot->tick(dt);
}

void World::run(double freq_hz, double duration_s) {
    double dt = 1/freq_hz;
    double duration_millis = 1e3*duration_s;
    int millis = dt*1000;
    double t0 = t();
    std::cout << "[World] Running simulation for " << duration_s << " seconds....\n";
    while( (t() - t0) < duration_millis) {
        tick(dt);
        std::this_thread::sleep_for(std::chrono::milliseconds(millis));
    }
}