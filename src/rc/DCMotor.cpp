#include "rc/DCMotor.hpp"

// Initialization of static variables
bool rc::DCMotor::rc_initialized(false);
int rc::DCMotor::nMotors(0);

rc::DCMotor::DCMotor(std::string name, int channel) : ::DCMotor(name), m_channel(channel), m_enabled(false),  m_limit(1.0) {
    nMotors += 1;
    if(!rc_initialized) {
        if(!initializeMotors()) {
            std::cout << "rc_motor_init failed!\n";
            return;
        }
    }
    m_enabled = true;
}

rc::DCMotor::~DCMotor() {
    nMotors -= 1;
    if(nMotors == 0) {
        if(shutDownMotors()) {
            std::cout << "All H-bridges put into standby and GPIO and PWM file pointers closed\n";
        } else {
            std::cout << "rc_motor_cleanup failed!\n";
        }
    }
}

bool rc::DCMotor::brake() const {
    return rc_motor_brake(m_channel);
}

bool rc::DCMotor::drive(double dutyCycle) const {
    if(dutyCycle < -1.0 || dutyCycle > 1.0) {
        std::cout << "Received duty cycle: " << dutyCycle << ". Absolute value of duty cycle cannot exceed 1.0!\n";
        return false;
    }
    double normalized = dutyCycle * m_limit;
    return rc_motor_set(m_channel, normalized) == 0;
}

bool rc::DCMotor::shutDownMotors() const {
    return rc_motor_cleanup() == 0;
}

bool rc::DCMotor::initializeMotors() const {
    return rc_motor_init() == 0;
}

void rc::DCMotor::setLimit(double limit) {
    m_limit = std::min(std::max(std::abs(limit), 0.0), 1.0);
}