#include "rc/IMU.hpp"

/* Initialization of static data of IMU */
Eigen::Vector4d rc::IMU::m_mean(Eigen::Vector4d::Zero());
Eigen::Vector4d rc::IMU::m_bias(Eigen::Vector4d::Zero());
std::queue<double> rc::IMU::m_accelXReadings(std::deque<double>(DEFAULT_NSAMPLES,0.0));
std::queue<double> rc::IMU::m_accelYReadings(std::deque<double>(DEFAULT_NSAMPLES,0.0));
std::queue<double> rc::IMU::m_thetaZReadings(std::deque<double>(DEFAULT_NSAMPLES,0.0)); // Angle
std::queue<double> rc::IMU::m_omegaZReadings(std::deque<double>(DEFAULT_NSAMPLES,0.0)); // Angular velocity
std::queue<double> rc::IMU::m_timeInstances(std::deque<double>(DEFAULT_NSAMPLES,0.0)); // Time deltas from most recent readings
Clock rc::IMU::m_clock = Clock();
double rc::IMU::m_dt(0.0);

rc_mpu_data_t rc::IMU::m_data;
rc_mpu_config_t rc::IMU::m_conf(rc_mpu_default_config());

rc::IMU::IMU() : m_clock() 
{

}

rc::IMU::setParams(json params) {
	try {
		m_sampleRate = params["sample_rate"].get<int>();
		} catch(std::exception& e) {
			std::cout << "[mock::IMU] Could not set sample_rate: " << e.what() << '\n'; 
		}
}

rc::IMU::~IMU() {
	rc_mpu_power_off();
}

bool rc::IMU::setup() {
	// Configuration settings
	m_conf.i2c_bus = I2C_BUS;
	m_conf.gpio_interrupt_pin_chip = GPIO_INT_PIN_CHIP;
	m_conf.gpio_interrupt_pin = GPIO_INT_PIN_PIN;
	m_conf.enable_magnetometer = 1;
	m_conf.dmp_fetch_accel_gyro = 1;
	m_conf.dmp_sample_rate = m_sampleRate;
	m_conf.accel_dlpf = ACCEL_DLPF_460;
	m_conf.accel_fsr = ACCEL_FSR_16G; 
	// Turn imu on dmp interrupt operation
	if(rc_mpu_initialize_dmp(&m_data, m_conf)){
		printf("rc_mpu_initialize_failed\n");
	        return false;
	}
	// TODO: implement non-dmp version
	rc_mpu_set_dmp_callback( &dmpCallback );	
	return true;
}

void rc::IMU::getReadings(Eigen::Vector4d& readings) {
	readings = m_mean - m_bias;
	// Integrate acceleration to get velocity
	readings(0) = 0;//-readings(0)*m_dt;
	readings(1) = 0;//-readings(1)*m_dt; // IMU points backwards in Y direction. 
}

void rc::IMU::autoCalibrate(double duration_ms, int numSamples) {
	double sampleInterval = duration_ms/numSamples;
	Eigen::MatrixX4d readings(numSamples,4);	
	double t0 = m_clock.t();
	double t = t0;
	int i = 0;
	while(  i < numSamples ) {
		if( t > (t0 + sampleInterval*(i+1)) ) {
			readings.row(i) = m_mean;
			i++;
		}
		t = m_clock.t();
	}
	m_bias = readings.colwise().mean();
	m_bias(2) = 0.0; // No ground truth for angle -> leave it be
}

void rc::IMU::dmpCallback(void) {
	// TODO: Less memory-intense, more efficient approach
	double newVal;
	// X acceleration
	newVal = m_data.accel[0];
	m_mean(0) = runningMean(m_accelXReadings.front(),newVal,m_mean(0),DEFAULT_NSAMPLES);
	m_accelXReadings.pop();
	m_accelXReadings.push(newVal);
	
	// Y acceleration
	newVal = m_data.accel[1];
	m_mean(1) = runningMean(m_accelYReadings.front(),newVal,m_mean(1),DEFAULT_NSAMPLES);
	m_accelYReadings.pop();
	m_accelYReadings.push(newVal);

	// Z theta
	newVal = m_data.compass_heading;
	m_mean(2) = runningMean(m_thetaZReadings.front(),newVal,m_mean(2),DEFAULT_NSAMPLES);
	m_thetaZReadings.pop();
	m_thetaZReadings.push(newVal);

	// Z omega
	newVal = m_data.gyro[2] * DEG_TO_RAD;
	m_mean(3) = runningMean(m_thetaZReadings.front(),newVal,m_mean(3),DEFAULT_NSAMPLES);
	m_thetaZReadings.pop();
	m_thetaZReadings.push(newVal);

	// dt
	newVal = m_clock.t();
	m_dt = newVal - m_timeInstances.front();
	m_timeInstances.pop();
	m_timeInstances.push(newVal);
}
