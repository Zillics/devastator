#include <iostream>
#include <chrono>
#include <ctime>
#include "gtest/gtest.h"
#include "mock/Controller.hpp"
#include "Logger.hpp"
#include "Parameters.hpp"
#include "Datatypes.hpp"
#include "json.hpp"
#include "Utils.hpp"

using json = nlohmann::json;
using namespace std::chrono;

std::string rootDir = PROJECT_DIR;
std::string conf1 = rootDir + "/params/mockController.json";
std::string conf2 = rootDir + "/params/mockNetwork.json";

std::shared_ptr<World> world = std::make_shared<World>(openJson(conf1)["World"]);
mock::Controller r(conf1, conf2, world);

TEST(testControlCmd,Smoketest) {
  ControlCmd cmd1;
  EXPECT_TRUE( cmd1.u(0) == 0 );
  EXPECT_TRUE( cmd1.u(1) == 0 );
  double testR = 0.23;
  double testL = -0.111;
  EXPECT_NO_THROW(cmd1.print());
  cmd1.u(0) = testL;
  cmd1.u(1) = testR;
  ControlCmd cmd2(testL,testR);
  EXPECT_TRUE( cmd2.u(0) == testL );
  EXPECT_TRUE( cmd2.u(1) == testR );
}

TEST(testSensorState,Smoketest) {
  SensorState s1;
  EXPECT_NO_THROW( s1.print() );
  EXPECT_TRUE(s1.angle() == 0.0);
  Eigen::Vector4d init = Eigen::Vector4d::Random();
  SensorState s2(init);
  EXPECT_TRUE(s2.z.isApprox(init));
}

TEST(testRobotState,Smoketest) {
  RobotState s1;
  EXPECT_NO_THROW( s1.print() );
  EXPECT_TRUE(s1.angle() == 0.0);
  EXPECT_TRUE(s1.angularVel() == 0.0);
  Eigen::Vector4d zInit = Eigen::Vector4d::Random();
  Eigen::Matrix<double,6,1> sInit = Eigen::Matrix<double,6,1>::Random();
  Eigen::Vector2d uInit = Eigen::Vector2d::Random();
  s1.z = zInit;
  s1.u = uInit;
  s1.s = sInit;
  EXPECT_TRUE(s1.z.isApprox(zInit));
  EXPECT_TRUE(s1.s.isApprox(sInit));
  EXPECT_TRUE(s1.u.isApprox(uInit));
}


TEST(testController,Smoketest) {
    // Test variables
    int freq_hz = 10;
    int duration_s = 1;
    int tol_micros = 200000;
    // Test
    EXPECT_TRUE( r.t() >= 0 );
    std::cout << "Running for 1s...." << std::endl;
    auto t1 = r.t();
    r.run(freq_hz,duration_s);
    auto t2 = r.t();
    EXPECT_TRUE( t2 > t1 );
    int error_micros = std::abs(t2 - t1 - duration_s * 1e6);
    std::cout << "BaseController::run, microsecond error: " << error_micros << '\n';
    EXPECT_TRUE( error_micros < tol_micros );
    std::cout << "Printing sensors..." << std::endl;
    r.printSensors();
    r.tick();
    r.tick();
}

