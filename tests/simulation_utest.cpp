#include "gtest/gtest.h"
#include <json.hpp>
#include "Utils.hpp"
#include "mock/World.hpp"
#include "mock/Simulation.hpp"
using json = nlohmann::json;

std::string rootDir = PROJECT_DIR;
std::string conf1 = rootDir + "/params/mockController.json";
std::string conf2 = rootDir + "/params/mockNetwork.json";
std::string simulationConf = rootDir + "/params/simulation.json";

TEST(testWorld, smokeTest) {
    // Test variables
    double tol_s = 0.1;
    int freq_hz = 100;
    int duration_s = 1;
    json params = openJson(conf1);

    // Test
    std::cout << "Testing World.run\n";
    Clock clock;
    World w = World(params["World"]);
    w.run(freq_hz, duration_s);
}

TEST(testSimulation, smokeTest) {
    // Test variables
    int duration_s = 5;
    // Test
    Simulation sim(simulationConf);
    sim.run(duration_s);
}