#include <iostream>
#include <chrono>
#include <ctime>
#include "gtest/gtest.h"
#include "mock/Controller.hpp"
#include "Logger.hpp"
#include "Parameters.hpp"
#include "Datatypes.hpp"
#include "json.hpp"
#include "Utils.hpp"

using json = nlohmann::json;
using namespace std::chrono;

std::string rootDir = PROJECT_DIR;
std::string conf1 = rootDir + "/params/mockController.json";
std::string conf2 = rootDir + "/params/mockNetwork.json";


TEST(testController,testTickDuration) {
  // Test functions
  auto measureTickDuration = [&](int N) {
    std::cout << "**********Measuring tick duration, N = " << N << "**********\n";
    double sumT = 0.0;
    for(int i = 0; i < N; i++) {
      auto t1 = high_resolution_clock::now();
      r.tick();
      auto t2 = high_resolution_clock::now();
      int micros = duration_cast<microseconds>(t2 - t1).count();
      sumT += (double)micros;
    }
    double mean = sumT / (double)N;
    std::cout << "Mean tick time: " << mean << " microseconds\n";
    return mean;
  };
  auto testTickAccuracy = [&](double T_s, double tol_micros) {
    std::cout << "**********Testing tick accuracy for interval " << T_s << "s, tolerance " << tol_micros <<" microseconds**********\n";
    auto t1 = high_resolution_clock::now();
    r.tick(T_s);
    auto t2 = high_resolution_clock::now();
    double micros = duration_cast<microseconds>(t2-t1).count();
    double T_micros = 1e6 * T_s;
    //double seconds = micros * 1e-6;
    double error_micros = std::abs(micros - T_micros);
    std::cout << "Error in microseconds: " << error_micros << '\n';
    EXPECT_TRUE(error_micros < tol_micros);
  };

  // Test variables
  int N = 10;
  double T_s = 0.001;
  double tol_micros = 1000;
  // Test
  double meanT = measureTickDuration(N);
  testTickAccuracy(T_s, tol_micros);

  int micros = 100;
  auto t1 = high_resolution_clock::now();
  std::this_thread::sleep_for(std::chrono::microseconds(micros));
  auto t2 = high_resolution_clock::now();
  auto tElapsed = duration_cast<std::chrono::microseconds>(t2-t1).count();
  std::cout << "micros: " << micros << ", elapsed: " << tElapsed << '\n';
}
