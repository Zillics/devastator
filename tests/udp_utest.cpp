#include <chrono>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <thread>
#include "gtest/gtest.h"
#include "NetUtils.hpp"
#include "Clock.hpp"
#include "Utils.hpp"
#include "Datatypes.hpp"

#define INTERVAL_MS 1000

std::string rootDir = PROJECT_DIR;
std::string configFilename = rootDir + "/tests/sender.json";

// Test whether opening json from file works
TEST(testUtils,smoketest) {
  EXPECT_ANY_THROW(openJson("/asfasgad/AAFGas7S/SDASfdasfa/AsdasdGAdhrhbav.json"));
  EXPECT_NO_THROW(openJson(configFilename));
}

TEST(testUdpPublisher,smoketest) {
    std::cout << "Creating UdpPublisher\n";
    UdpPublisher pub(configFilename);
    std::cout << "Starting to publish\n";
    for(unsigned int i = 0; i < 4; i++) {
        std::cout << "Publishing " << i << "\n";
        pub.publish("Publishing over UDP successful!");
        std::cout << "Published. Going to sleep\n";
        std::this_thread::sleep_for(std::chrono::milliseconds(INTERVAL_MS));
    }
}

TEST(testSerialization, testControlCmd) {
  // Create datatype
  ControlCmd cmd;

  // Set some dummy values
  cmd.u = Eigen::Vector2d::Random();
  std::cout << "Original ControlCmd: \n";
  cmd.print();
  // Create an output archive
  std::ofstream ofs( "cmd.dat" );
  boost::archive::text_oarchive oar(ofs);

  // Save the data
  oar & cmd;
  ofs.close();

  // Read from file
  ControlCmd cmd2;
  std::ifstream ifs("cmd.dat");
  boost::archive::text_iarchive iar(ifs);
  iar & cmd2;

  // Test
  std::cout << "ControlCmd after serialization: \n";
  cmd2.print();
  EXPECT_TRUE(cmd.u(0) == cmd2.u(0));
  EXPECT_TRUE(cmd.u(1) == cmd2.u(1));
}

TEST(testSerialization, testSensorState) {
  // Set some dummy values
  Eigen::Vector4d ref = Eigen::Vector4d::Random();
  SensorState sensor1(ref);
  std::cout << "Original SensorState: \n";
  sensor1.print();
  // Create an output archive
  std::ofstream ofs( "sensor.dat" );
  boost::archive::text_oarchive oar(ofs);

  // Save the data
  oar & sensor1;
  ofs.close();

  // Read from file
  SensorState sensor2;
  std::ifstream ifs("sensor.dat");
  boost::archive::text_iarchive iar(ifs);
  iar & sensor2;

  // Test
  std::cout << "SensorState after serialization: \n";
  sensor2.print();
  EXPECT_TRUE(sensor2.z.isApprox(ref));
}

TEST(testSerialization, testRobotState) {
  // Set some dummy values
  Eigen::MatrixXd s_ref = Eigen::MatrixXd::Random(6,1);
  Eigen::MatrixXd u_ref = Eigen::MatrixXd::Random(2,1);
  Eigen::MatrixXd z_ref = Eigen::MatrixXd::Random(4,1);
  RobotState state1;
  state1.s = s_ref;
  state1.u = u_ref;
  state1.z = z_ref;
  // Create an output archive
  std::ofstream ofs( "state.dat" );
  boost::archive::text_oarchive oar(ofs);

  // Save the data
  oar & state1;
  ofs.close();

  // Read from file
  RobotState state2;
  std::ifstream ifs("state.dat");
  boost::archive::text_iarchive iar(ifs);
  iar & state2;

  // Test
  std::cout << "Retrieved RobotState: \n";
  state2.print();
  EXPECT_TRUE(state2.s.isApprox(s_ref));
  EXPECT_TRUE(state2.u.isApprox(u_ref));
  EXPECT_TRUE(state2.z.isApprox(z_ref));
}

TEST(testUdpSubscriber, smokeTest) {
  ControlCmd cmd;
  SensorState sensor;
  RobotState state;
  UdpSubscriber sub(configFilename);
  EXPECT_FALSE(sub.hasReceived());
  EXPECT_FALSE(sub.read(cmd));
  EXPECT_FALSE(sub.read(sensor));
  EXPECT_FALSE(sub.read(state));
  EXPECT_NO_THROW(sub.start());
  EXPECT_NO_THROW(sub.stop()); 
}


TEST(testPubSub, testControlCmd) {
  Clock clock;
  UdpSubscriber sub(configFilename);
  sub.start();
  UdpPublisher pub(configFilename);
  // Set some dummy values
  double l = 0.4382;
  double r = 0.1245;
  ControlCmd cmd1(l,r);
  std::cout << "Sending ControlCmd: \n";
  cmd1.print();
  bool received = false;
  bool timeout = false;
  uint64_t timeout_ms = 2000;
  std::cout << "Setting timeout " << timeout_ms << " ms\n";
  ControlCmd cmd2;
  clock.tic(0);
  while( !timeout && !received ) {
    pub.publish(cmd1);
    if(sub.hasReceived()) {
      received = true;
      sub.read(cmd2);
      std::cout << "Received ControlCmd: \n";
      cmd2.print();
    }
    timeout = clock.toc(0) > timeout_ms * 1e3;
  }
  EXPECT_FALSE(timeout);
  EXPECT_TRUE(received);
  EXPECT_TRUE(cmd2.u(0) == cmd1.u(0));
  EXPECT_TRUE(cmd2.u(1) == cmd1.u(1));
}

TEST(testPubSub, testSensorState) {
  Clock clock;
  UdpSubscriber sub(configFilename);
  sub.start();
  UdpPublisher pub(configFilename);
  // Set some dummy values
  Eigen::Vector4d ref = Eigen::Vector4d::Random();
  SensorState sensor1(ref);
  std::cout << "Sending SensorState: \n";
  sensor1.print();
  bool received = false;
  bool timeout = false;
  uint64_t timeout_ms = 2000;
  std::cout << "Setting timeout " << timeout_ms << " ms\n";
  SensorState sensor2;
  clock.tic(0);
  while( !timeout && !received ) {
    pub.publish(sensor1);
    if(sub.hasReceived()) {
      received = true;
      sub.read(sensor2);
      std::cout << "Received SensorState: \n";
      sensor2.print();
    }
    timeout = clock.toc(0) > timeout_ms * 1e3;
  }
  EXPECT_FALSE(timeout);
  EXPECT_TRUE(received);
  EXPECT_TRUE(sensor2.z.isApprox(ref));
}

TEST(testPubSub, testRobotState) {
  Clock clock;
  UdpSubscriber sub(configFilename);
  sub.start();
  UdpPublisher pub(configFilename);
  // Set some dummy values
  Eigen::Matrix<double,6,1> _s = Eigen::Matrix<double,6,1>::Random();
  Eigen::Vector2d _u = Eigen::Vector2d::Random();
  Eigen::Vector4d _z = Eigen::Vector4d::Random();
  RobotState state;
  state.s = _s;
  state.u = _u;
  state.z = _z;
  std::cout << "Sending RobotState:\n";
  state.print();
  bool received = false;
  bool timeout = false;
  double timeout_ms = 2000;
  std::cout << "Setting timeout " << timeout_ms << " ms\n";
  RobotState recvState;
  clock.tic(0);
  while( !timeout && !received ) {
    pub.publish(state);
    if(sub.hasReceived()) {
      received = true;
      sub.read(recvState);
      std::cout << "Received RobotState:\n";
      recvState.print();
    }
    timeout = clock.toc(0) > timeout_ms * 1e3;
  }
  EXPECT_FALSE(timeout);
  EXPECT_TRUE(received);
  EXPECT_TRUE(recvState.s.isApprox(_s));
  EXPECT_TRUE(recvState.u.isApprox(_u));
  EXPECT_TRUE(recvState.z.isApprox(_z));
}